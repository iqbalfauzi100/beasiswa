###################
SI BEASISWA UIN MALIKI MALANG
###################

Sistem buat daftar beasiswa
*******************
Release Information
*******************

This repo contains in-development code for future releases.

**************************
Changelog and New Features
**************************


*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.


*******
License
*******

Please see SantriDev Agreement
***************
Acknowledgement
***************

The SantriDev team would like to thank MrFahudi, all the
contributors to the SIBea project, SI Bea user.
