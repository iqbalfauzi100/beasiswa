-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 28 Mei 2018 pada 03.16
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beasiswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman_upload`
--

CREATE TABLE `pengumuman_upload` (
  `id` int(3) NOT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `judul` text,
  `tanggal` date DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman_upload`
--

INSERT INTO `pengumuman_upload` (`id`, `jenis`, `judul`, `tanggal`, `file`) VALUES
(42, 'Kepengawasan', 'Surat-surat yang berkenaan dengan kepengawasan pada SLTP/Tsanawiyah, SLTA/Aliyah, pondok pesantren', '2018-05-20', 'DAFTAR_PUSTAKA.docx');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pengumuman_upload`
--
ALTER TABLE `pengumuman_upload`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pengumuman_upload`
--
ALTER TABLE `pengumuman_upload`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
