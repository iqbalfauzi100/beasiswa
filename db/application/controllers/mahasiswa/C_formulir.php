<?php defined('BASEPATH')OR exit('akses tidak dapat diterima');

class C_formulir extends CI_Controller
{

  function __construct(){
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    $this->load->model("mahasiswa/Formulir",'mdl');

    $this->load->library('upload');
    $this->load->helper(array('url'));
  }

  public function index()
  {
    $nim = $this->session->userdata('username');
    $idBea = $this->input->post('idPengaturan');

    $dataMhs = $this->mdl->getdataMhs($nim);
    $data = array(
      'idBea' => $idBea,
      'namaBea' => $this->mdl->get_nama_bea($idBea),
      'combo' => $this->mdl->get_skor_bea($idBea),
      'jurusan' => $this->mdl->get_jurusan(),
      'nim' => $dataMhs->nimMhs,
      'nama' => $dataMhs->namaLengkap,
      'tempatLahir' => $dataMhs->tempatLahir,
      'tglLahir' => $dataMhs->tglLahir,
      'asalKota' => $dataMhs->asalKota,
      'noTelp' => $dataMhs->noTelp
      );
    $this->load->view('mahasiswa/formulir', $data);
  }
  public function cekNim($nim, $idBea)
  {
    $data = $this->mdl->cekNim($nim,$idBea);

    if ($data != 0){
      redirect('mahasiswa/C_daftar_bea');
    }else{
      $this->simpan();
    }
  }
  public function simpan()
  {
    $nim  = $this->input->post('nim');
    $data =array(
      'idBea'     => $this->input->post('idBea'),
      'nim'       => $this->input->post('nim'),
      'semester'  => $this->input->post('semester'),
      'sks'       => $this->input->post('sks'),
      'ipk'       => $this->input->post('ipk'),
      'alamatMalang' => $this->input->post('alamatMalang'),
      'tanggal'   => $this->input->post('tanggal')
      );
    $data1 = $this->mdl->cekNim($this->input->post('nim'),$this->input->post('idBea'));
    if ($data1 != 0){
      redirect('mahasiswa/C_daftar_bea');
    }else{
      $pendaftar = $this->mdl->getInsert($data);

      // echo(json_encode($this->input->post('idKategoriSkor')));
      // echo(json_encode($this->input->post('score')));

      $count_score = count($this->input->post('idKategoriSkor'));
      $data1 = array();
      for ($i=0;$i<$count_score;$i++) {
        $kategori_skor = $this->input->post('idKategoriSkor['.$i.']');
        $skor          = $this->input->post('score['.$i.']');
        $data1[]= array(
          'idKategori'    => $kategori_skor,
          'idSubKategori' => $skor,
          'idBea'         => $this->input->post('idBea'),
          'idPendaftar'   => $pendaftar
          );
      }
      $this->mdl->pendaftarSkor($data1);

      if($this->input->post('action1') && !empty($_FILES['fileBerkas']['name'])){
        $filesCount = count($_FILES['fileBerkas']['name']);
        for($i = 0; $i < $filesCount; $i++){
          $_FILES['fileberkas']['name'] = $_FILES['fileBerkas']['name'][$i];
          $_FILES['fileberkas']['type'] = $_FILES['fileBerkas']['type'][$i];
          $_FILES['fileberkas']['tmp_name'] = $_FILES['fileBerkas']['tmp_name'][$i];
          $_FILES['fileberkas']['error'] = $_FILES['fileBerkas']['error'][$i];
          $_FILES['fileberkas']['size'] = $_FILES['fileBerkas']['size'][$i];

          $uploadPath = 'assets/img/upload_berkas/';
          $config['upload_path'] = $uploadPath;
          // $config['allowed_types'] = '*';
          $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|docx|doc';
          $config['file_name'] = "file_".time();

          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          if($this->upload->do_upload('fileberkas')){
            $fileData = $this->upload->data();
            $uploadData[$i]['title'] = $fileData['file_name'];
            $uploadData[$i]['type'] = $fileData['file_type'];
            $uploadData[$i]['ukuran'] = $fileData['file_size'];
            $uploadData[$i]['created'] = date("Y-m-d H:i:s");
            $uploadData[$i]['idBerkas'] = $this->input->post('idBerkas['.$i.']');
            $uploadData[$i]['idBea'] = $this->input->post('idBea');
            $uploadData[$i]['idPendaftar'] = $pendaftar;
          }
        }
      }
      $this->mdl->pendaftarUpload($uploadData);
      redirect('mahasiswa/C_daftar_bea');
    }
    // $pendaftar = $this->mdl->getInsert($data);
    //
    // // echo(json_encode($this->input->post('idKategoriSkor')));
    // // echo(json_encode($this->input->post('score')));
    //
    // $count_score = count($this->input->post('idKategoriSkor'));
    // $data1 = array();
    // for ($i=0;$i<$count_score;$i++) {
    //   $kategori_skor = $this->input->post('idKategoriSkor['.$i.']');
    //   $skor          = $this->input->post('score['.$i.']');
    //   $data1[]= array(
    //     'idKategori'    => $kategori_skor,
    //     'idSubKategori' => $skor,
    //     'idBea'         => $this->input->post('idBea'),
    //     'idPendaftar'   => $pendaftar
    //     );
    // }
    // $this->mdl->pendaftarSkor($data1);
    //
    // if($this->input->post('action1') && !empty($_FILES['fileBerkas']['name'])){
    //   $filesCount = count($_FILES['fileBerkas']['name']);
    //   for($i = 0; $i < $filesCount; $i++){
    //     $_FILES['fileberkas']['name'] = $_FILES['fileBerkas']['name'][$i];
    //     $_FILES['fileberkas']['type'] = $_FILES['fileBerkas']['type'][$i];
    //     $_FILES['fileberkas']['tmp_name'] = $_FILES['fileBerkas']['tmp_name'][$i];
    //     $_FILES['fileberkas']['error'] = $_FILES['fileBerkas']['error'][$i];
    //     $_FILES['fileberkas']['size'] = $_FILES['fileBerkas']['size'][$i];
    //
    //     $uploadPath = 'assets/img/upload_berkas/';
    //     $config['upload_path'] = $uploadPath;
    //     // $config['allowed_types'] = '*';
    //     $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|docx|doc';
    //     $config['file_name'] = "file_".time();
    //
    //     $this->load->library('upload', $config);
    //     $this->upload->initialize($config);
    //
    //     if($this->upload->do_upload('fileberkas')){
    //       $fileData = $this->upload->data();
    //       $uploadData[$i]['title'] = $fileData['file_name'];
    //       $uploadData[$i]['type'] = $fileData['file_type'];
    //       $uploadData[$i]['ukuran'] = $fileData['file_size'];
    //       $uploadData[$i]['created'] = date("Y-m-d H:i:s");
    //       $uploadData[$i]['idBerkas'] = $this->input->post('idBerkas['.$i.']');
    //       $uploadData[$i]['idBea'] = $this->input->post('idBea');
    //       $uploadData[$i]['idPendaftar'] = $pendaftar;
    //     }
    //   }
    // }
    // $this->mdl->pendaftarUpload($uploadData);
    // redirect('mahasiswa/C_daftar_bea');
  }
  public function simpan2()
  {
    $nim  = $this->input->post('nim');
    $data =array(
      'idBea'     => $this->input->post('idBea'),
      'nim'       => $this->input->post('nim'),
      'semester'  => $this->input->post('semester'),
      'sks'       => $this->input->post('sks'),
      'ipk'       => $this->input->post('ipk'),
      'alamatMalang' => $this->input->post('alamatMalang'),
      'tanggal'   => $this->input->post('tanggal')
      );

    $this->mdl->getInsert($data);

        // echo(json_encode($this->input->post('idKategoriSkor')));
        // echo(json_encode($this->input->post('score')));
        // echo json_encode(array("score" => TRUE));
    redirect('mahasiswa/C_daftar_bea');
  }

  function upload_image(){
      $config['upload_path'] = './assets/images/'; //path folder
      $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
      // $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
      $config['max_size'] = '5000';
      $config['max_width']  = '5000';
      $config['max_height']  = '5000';

      $this->load->library('upload',$config);

      $count_upload = count($this->input->post('idBerkas'));
      $data_upload  = array();
      for ($i=1; $i <= $count_upload ; $i++) {
        $id_berkas  = $this->input->post('idBerkas['.$i.']');
        $berkas     = $this->input->post('fileberkas['.$i.']');

        if(!empty($_FILES['fileberkas'.$i]['name'])){
          if(!$this->upload->do_upload('fileberkas'.$i)){
            $this->upload->display_errors();
          }
          else{
            $data_upload[]= array(
              'ukuran' => $_FILES['fileberkas']['size'],
              'type' => $_FILES['fileberkas']['type'],
              'title' => $_FILES['fileberkas']['name'],
              'idBerkas'      => $id_berkas,
              'idBea'         => $this->input->post('idBea'),
              'idPendaftar'   => $pendaftar
              );
          }
        }
      }
      $this->mdl->pendaftarUpload($data_upload);
    }
  }


/* if(!empty($_FILES['fileberkas'.$i]['name'])){
          if(!$this->upload->do_upload('fileberkas'.$i)){
            $this->upload->display_errors();
          }
          else{
            $size=$_FILES['fileberkas']['size'];
            $type=$_FILES['fileberkas']['type'];
            $title=$_FILES['fileberkas']['name'];
            $data_upload[]= array(
              'ukuran' => $size,
              'type' => $type,
              'title' => $title,
              'idBerkas'      => $id_berkas,
              'idBea'         => $this->input->post('idBea'),
              'idPendaftar'   => $pendaftar
              );
              echo "Foto berhasil di upload<br>";
            }
          }*/
