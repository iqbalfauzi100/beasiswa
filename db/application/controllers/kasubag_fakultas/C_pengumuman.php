<?php defined('BASEPATH')OR exit('tidak ada akses diizinkan');
class C_pengumuman extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    $this->load->model("kasubag/Pengumuman_upload",'model');
    
  }

  public function index()
  {
    $this->load->view('attribute/header_kasubag_fk');
    $this->load->view('kasubag_fk/pengumuman');
    $this->load->view('attribute/footer');
  }
  public function viewPengumuman()
  {
    $fetch_data = $this->model->make_datatables();
    $data = array();
    $nmr = 0;
    foreach($fetch_data as $row)
    {
      $nmr+=1;
      $sub_array = array();
      $sub_array[] = $nmr;
      $sub_array[] = $row->jenis;
      $sub_array[] = $row->judul;
      $sub_array[] = $row->tanggal;
      $sub_array[] = '
      <b>File : </b><i><a href="'.base_url().'assets/img/upload_pengumuman/'.$row->file.'" target="_blank">'.$row->file.'</a>
      ';
      $sub_array[] = '
      <button type="button" name="edit" id="'.$row->id.'" onclick="edit('."'".$row->id."'".')" class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="mdi-editor-mode-edit"></i></button>
      <button type="button" name="remove" id="'.$row->id.'" onclick="remove('."'".$row->id."','".$row->judul."'".')" class="btn-floating waves-effect waves-light red" title="Hapus"><i class="mdi-action-delete">delete</i></button>
      ';
      $data[] = $sub_array;
    }
    $output = array(
      "draw"            =>  intval($_POST["draw"]),
      "recordsTotal"    =>  $this->model->get_all_data(),
      "recordsFiltered" =>  $this->model->get_filtered_data(),
      "data"            =>  $data
      );
    echo json_encode($output);
  }
}
?>
