-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 27 Mei 2018 pada 06.25
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beasiswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `userId` varchar(12) DEFAULT NULL,
  `password` text,
  `idLevel` int(11) DEFAULT NULL,
  `status` enum('open','close') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `userId`, `password`, `idLevel`, `status`) VALUES
(25, '1', '1', 1, 'open'),
(26, '2', '2', 2, 'open'),
(27, '3', '3', 3, 'open'),
(28, '4', '4', 4, 'open'),
(29, '5', '5', 5, 'open'),
(34, '6', '6', 6, 'open'),
(35, '15650025', '15650025', 5, 'open'),
(36, '15650026', '15650026', 5, 'open'),
(37, '15650027', '15650027', 5, 'open');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bea`
--

CREATE TABLE `bea` (
  `id` int(11) NOT NULL,
  `namaBeasiswa` varchar(100) DEFAULT NULL,
  `penyelenggaraBea` varchar(100) DEFAULT NULL,
  `kuota` char(10) DEFAULT NULL,
  `beasiswaDibuka` date DEFAULT NULL COMMENT 'pendaftaran',
  `beasiswaTutup` datetime DEFAULT NULL COMMENT 'pendaftaran',
  `periodeBerakhir` date DEFAULT NULL,
  `statusBeasiswa` enum('2','3') DEFAULT '2' COMMENT '2=ditolak, 3=dikonfirmasi',
  `seleksiTutup` date NOT NULL,
  `selektor` int(1) NOT NULL COMMENT '1=kasubag kemahasiswaa, 2=kasubag fakultas, 3=keduanya',
  `selektorFakultas` int(5) DEFAULT NULL COMMENT 'berisi idFakultas',
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bea`
--

INSERT INTO `bea` (`id`, `namaBeasiswa`, `penyelenggaraBea`, `kuota`, `beasiswaDibuka`, `beasiswaTutup`, `periodeBerakhir`, `statusBeasiswa`, `seleksiTutup`, `selektor`, `selektorFakultas`, `keterangan`) VALUES
(17, 'Bank Indonesia 2018', 'Bank Indonesia', '50', '2017-02-25', '2018-03-01 00:00:00', '2019-02-25', '3', '2018-03-05', 1, NULL, NULL),
(18, 'Bank BRI', 'Bank BRI', '50', '2018-02-25', '2018-03-01 00:00:00', '2019-02-25', '3', '2018-03-05', 1, NULL, NULL),
(19, 'Bank BTN', 'Bank BTN', '50', '2018-02-25', '2018-05-30 16:35:00', '2019-02-25', '2', '2018-03-05', 1, NULL, NULL),
(20, 'Beasiswa Putra Daerah', 'Pemkab Jembrana', '50', '2018-05-01', '2018-05-25 16:30:00', '2018-12-31', '3', '2018-05-31', 1, NULL, ''),
(22, 'Beasiswa BidikMisi', 'Pemerintah Indonesia', '40', '2018-05-02', '2018-05-21 15:32:00', '2019-01-31', '2', '2018-05-31', 1, NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `idBerita` int(11) NOT NULL,
  `judulBerita` text,
  `topikBerita` text,
  `penulisBerita` varchar(100) DEFAULT NULL,
  `kontenBerita` text,
  `tglInBerita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`idBerita`, `judulBerita`, `topikBerita`, `penulisBerita`, `kontenBerita`, `tglInBerita`) VALUES
(1, 'SI BEA', 'Sistem Informasi Pendaftaran Beasiswa UIN Malang', 'Iqbal Fauzi', '<p xss=removed>Pada penghujung akhir tahun 2017, Subbag. Kemahasiswaan meluncurkan sistem informasi penerimaan beasiswa baru yang diberi nama <strong>SIBEA</strong>, sebagai pengembangan dari sistem sebelumnya. Pada sistem ini terjadi banyak perubahan baik dari segi Layout, manajemen penerimaan, user level, dan juga report yang ada.</p>\r\n\r\n<p xss=removed>Harapan kedepannya semoga sistem yang baru dapat membawa kemudahan pengelolaan, lebih cepat pengelolaan, transparansi, dan kemudahan akses bagi civitas akademika UIN Maulana Malik Ibrahim Malang.</p>\r\n\r\n<p xss=removed>Kritik dan saran yang membangun sangat kami harapkan dari seluruh civitas akademika UIN Maulana MalikIbrahim Malang, guna meningkatkan pelayanan kami pada semua.</p>\r\n', '2018-02-27 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berkas_upload`
--

CREATE TABLE `berkas_upload` (
  `id` int(11) NOT NULL,
  `namaBerkas` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berkas_upload`
--

INSERT INTO `berkas_upload` (`id`, `namaBerkas`) VALUES
(1, 'SKKB'),
(2, 'Surat Keterangan Sehat'),
(3, 'SKCK'),
(4, 'Surat Keterangan Miskin'),
(5, 'Surat Keterangan Domisili');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE `fakultas` (
  `id` int(11) NOT NULL,
  `namaFk` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`id`, `namaFk`) VALUES
(1, 'Tarbiyah dan Ilmu Keguruan'),
(2, 'Syari’ah'),
(3, 'Humaniora'),
(4, 'Psikologi'),
(5, 'Ekonomi'),
(6, 'Sains dan Teknologi'),
(7, 'Kedokteran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `identitas_mhs`
--

CREATE TABLE `identitas_mhs` (
  `nimMhs` int(11) NOT NULL,
  `namaLengkap` varchar(100) DEFAULT NULL,
  `tempatLahir` text,
  `tglLahir` date DEFAULT NULL,
  `asalKota` varchar(20) DEFAULT NULL,
  `namaOrtu` varchar(20) DEFAULT NULL,
  `alamatOrtu` text,
  `kotaOrtu` varchar(30) DEFAULT NULL,
  `propinsiOrtu` varchar(30) DEFAULT NULL,
  `angkatan` char(5) DEFAULT NULL,
  `jenisKel` tinyint(1) DEFAULT NULL,
  `alamatLengkap` text,
  `emailAktif` varchar(100) DEFAULT NULL,
  `noTelp` char(12) DEFAULT NULL,
  `fotoMhs` varchar(100) DEFAULT NULL,
  `idJrs` int(11) DEFAULT NULL,
  `idAkses` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `identitas_mhs`
--

INSERT INTO `identitas_mhs` (`nimMhs`, `namaLengkap`, `tempatLahir`, `tglLahir`, `asalKota`, `namaOrtu`, `alamatOrtu`, `kotaOrtu`, `propinsiOrtu`, `angkatan`, `jenisKel`, `alamatLengkap`, `emailAktif`, `noTelp`, `fotoMhs`, `idJrs`, `idAkses`) VALUES
(5, 'Iqbal fauzi', 'Malang', '2018-02-28', 'Malang', 'Ali Bahrodin', 'Malang', 'Malang', 'Jawa Timur', '2005', 2, 'Malang', 'iqbalfauzi110@gmail.com', '085765768975', 'file_1524930951.jpg', 2, 29),
(15650025, 'Iqbal Fauzi', 'Malang', '2017-10-25', 'Malang', 'Ali', 'Malang', 'Malang', 'Jawa Timur', '2015', NULL, 'Malang', 'iqbal@gmail.com', '085765478765', 'file_1519075656.jpg', 7, 35),
(15650026, 'Achmad Wibowo', 'Malang', '2017-08-01', 'Malang', 'Muh', 'Malang', 'Malang', 'Jawa Timur', '2017', 1, 'Malang', 'Muhammad@gmail.com', '085756787654', 'file_1503562985.png', 6, 29),
(15650027, 'Muhammad Muslim', 'Bima', '1996-05-07', 'Bima', 'Bima', 'Bima', 'Bima', 'Bima', '2015', 2, 'Bima', 'muslim@gmail.com', '085456787653', 'file_1527089062.png', 2, 37);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(11) NOT NULL,
  `namaJur` varchar(100) DEFAULT NULL,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id`, `namaJur`, `idFk`) VALUES
(2, 'Teknik Informatika', 6),
(3, 'Pendidikan Agama Islam', 1),
(4, 'Pendidikan IPS', 1),
(5, 'Pendidikan Guru Madrasah Ibtidaiyah (PGMI)', 1),
(6, 'Pendidikan Bahasa Arab', 1),
(7, 'Pendidikan Islam Anak Usia Dini (PIAUD)', 1),
(8, 'Manajemen Pendidikan Islam', 1),
(9, 'Al-Ahwal Al-Syakhsiyah', 2),
(10, 'Hukum Bisnis Syari\'ah', 2),
(11, 'Hukum Tata Negara', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_skor`
--

CREATE TABLE `kategori_skor` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_skor`
--

INSERT INTO `kategori_skor` (`id`, `nama`) VALUES
(1, 'PLN'),
(2, 'Penghasilan Ayah'),
(3, 'Pekerjaan Ayah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `levellog`
--

CREATE TABLE `levellog` (
  `id` int(11) NOT NULL,
  `level` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `levellog`
--

INSERT INTO `levellog` (`id`, `level`) VALUES
(1, 'Staff Kemahasiswaan'),
(2, 'Kasubag'),
(3, 'Kasubag Fakultas'),
(4, 'Kabag'),
(5, 'Mahasiswa'),
(6, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftar`
--

CREATE TABLE `pendaftar` (
  `id` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `semester` int(1) NOT NULL,
  `sks` int(2) NOT NULL,
  `ipk` float NOT NULL,
  `alamatMalang` text NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '1=diterima, 0=tidak_diterima',
  `waktuDiubah` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pendaftar`
--

INSERT INTO `pendaftar` (`id`, `idBea`, `nim`, `semester`, `sks`, `ipk`, `alamatMalang`, `tanggal`, `status`, `waktuDiubah`) VALUES
(31, 22, 15650025, 1, 3, 3, 'ml', '2018-05-07', 1, '2018-05-23 14:35:42'),
(33, 22, 5, 8, 5, 5, 'malang', '2018-05-22', 1, '2018-05-23 14:35:43'),
(34, 22, 15650027, 7, 4, 4, 'Bima', '2018-05-23', 0, '2018-05-23 15:42:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftar_skor`
--

CREATE TABLE `pendaftar_skor` (
  `id` int(40) NOT NULL,
  `idPendaftar` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `idSubKategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pendaftar_skor`
--

INSERT INTO `pendaftar_skor` (`id`, `idPendaftar`, `idBea`, `idKategori`, `idSubKategori`) VALUES
(36, 31, 22, 1, 1),
(37, 31, 22, 2, 8),
(38, 33, 22, 1, 2),
(39, 33, 22, 2, 8),
(40, 34, 22, 1, 2),
(41, 34, 22, 2, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftar_upload`
--

CREATE TABLE `pendaftar_upload` (
  `id` int(11) NOT NULL,
  `idPendaftar` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `idBerkas` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `type` varchar(1000) NOT NULL,
  `ukuran` varchar(100) NOT NULL,
  `created` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `pendaftar_upload`
--

INSERT INTO `pendaftar_upload` (`id`, `idPendaftar`, `idBea`, `idBerkas`, `title`, `type`, `ukuran`, `created`) VALUES
(21, 31, 22, 1, 'Selection_002.png', 'image/png', '22.49', '2018-05-07 17:56:35'),
(22, 31, 22, 2, 'Foto_IQBAL_FAUZI.JPG', 'image/jpeg', '814.26', '2018-05-07 17:56:35'),
(23, 31, 22, 3, 'Foto_Merah_IQBAL_FAUZI.jpg', 'image/jpeg', '732.27', '2018-05-07 17:56:35'),
(24, 31, 22, 4, '-PPAH-_LOGO.png', 'image/png', '3384.58', '2018-05-07 17:56:35'),
(25, 33, 22, 1, 'Selection_001.png', 'image/png', '8.29', '2018-05-22 11:24:34'),
(26, 33, 22, 2, 'Selection_002.png', 'image/png', '12.47', '2018-05-22 11:24:34'),
(27, 33, 22, 3, 'Selection_003.png', 'image/png', '22.49', '2018-05-22 11:24:34'),
(28, 33, 22, 4, 'Selection_004.png', 'image/png', '83.54', '2018-05-22 11:24:34'),
(29, 34, 22, 1, 'file_1527089764.png', 'image/png', '8.29', '2018-05-23 17:36:04'),
(30, 34, 22, 2, 'file_15270897641.png', 'image/png', '12.47', '2018-05-23 17:36:04'),
(31, 34, 22, 3, 'file_15270897642.png', 'image/png', '22.49', '2018-05-23 17:36:04'),
(32, 34, 22, 4, 'file_15270897643.png', 'image/png', '83.54', '2018-05-23 17:36:04');

-- --------------------------------------------------------

--
-- Stand-in structure for view `penerima_bea`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `penerima_bea` (
`nimMhs` int(11)
,`namaLengkap` varchar(100)
,`idBea` int(11)
,`namaBeasiswa` varchar(100)
,`ipk` float
,`skor` decimal(32,0)
,`jumlah` double
,`status` int(1)
,`beasiswaDibuka` date
,`beasiswaTutup` datetime
,`periodeBerakhir` date
,`now` date
,`berakhirPendaftaran` int(7)
,`berakhirPeriode` int(7)
,`updated` timestamp
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman_upload`
--

CREATE TABLE `pengumuman_upload` (
  `id` int(3) NOT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman_upload`
--

INSERT INTO `pengumuman_upload` (`id`, `jenis`, `judul`, `tanggal`, `file`) VALUES
(42, 'Kepengawasan', 'Surat-surat yang berkenaan dengan kepengawasan pada SLTP/Tsanawiyah, SLTA/Aliyah, pondok pesantren d', '2018-05-20', 'DAFTAR_PUSTAKA.docx');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_admin`
--

CREATE TABLE `profil_admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `noTelp` char(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `idFakultas` int(11) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil_admin`
--

INSERT INTO `profil_admin` (`id`, `nama`, `alamat`, `noTelp`, `email`, `idAkses`, `idFakultas`, `foto`) VALUES
(31, 'Iqbal Fauzi', 'Malang', '085735059741', 'iqbal@gmail.com', 25, NULL, 'file_1526957789.png'),
(32, 'Tarbiyah', 'Malang', '085746987543', 'tarbiyah@gmail.com', 27, 6, 'file_1519075282.png'),
(33, 'admin', 'admin', '099', 'admin@gmail.com', 34, NULL, 'file_1519075718.jpg'),
(34, 'kasubag', 'kasubag', '099', 'kasubag@gmail.com', 26, NULL, 'file_1526981639.png'),
(35, 'kabag', 'kabag', '099', 'kabag@gmail.com', 28, NULL, 'file_1526028956.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_bea_berkas_upload`
--

CREATE TABLE `set_bea_berkas_upload` (
  `id` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `idBerkas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `set_bea_berkas_upload`
--

INSERT INTO `set_bea_berkas_upload` (`id`, `idBea`, `idBerkas`) VALUES
(5, 22, 1),
(6, 22, 2),
(11, 22, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_bea_kategori_skor`
--

CREATE TABLE `set_bea_kategori_skor` (
  `id` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `idKategoriSkor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `set_bea_kategori_skor`
--

INSERT INTO `set_bea_kategori_skor` (`id`, `idBea`, `idKategoriSkor`) VALUES
(29, 22, 1),
(33, 22, 2),
(34, 22, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_sub_kategori_skor`
--

CREATE TABLE `set_sub_kategori_skor` (
  `id` int(11) NOT NULL,
  `idKategoriSkor` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `skor` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `set_sub_kategori_skor`
--

INSERT INTO `set_sub_kategori_skor` (`id`, `idKategoriSkor`, `nama`, `skor`) VALUES
(1, 1, '<450', 1),
(2, 1, '450-750', 2),
(3, 1, '750-900', 3),
(4, 1, '900-1000', 4),
(8, 2, '700-900rb', 3),
(9, 2, '900-1000rb', 4),
(13, 2, '100-700', 2),
(14, 3, 'PNS/Pegawai', 1),
(15, 3, 'Karyawan', 2),
(16, 3, 'Pedagang', 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `skor_mahasiswa`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `skor_mahasiswa` (
`nimMhs` int(11)
,`namaLengkap` varchar(100)
,`idPendaftar` int(11)
,`idBeasiswa` int(11)
,`namaBeasiswa` varchar(100)
,`ipk` float
,`skor` decimal(32,0)
,`jumlah` double
,`status` int(1)
,`updated` timestamp
);

-- --------------------------------------------------------

--
-- Struktur untuk view `penerima_bea`
--
DROP TABLE IF EXISTS `penerima_bea`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `penerima_bea`  AS  select `pendaftar`.`nim` AS `nimMhs`,`identitas_mhs`.`namaLengkap` AS `namaLengkap`,`pendaftar`.`idBea` AS `idBea`,`bea`.`namaBeasiswa` AS `namaBeasiswa`,`pendaftar`.`ipk` AS `ipk`,(select sum(`set_sub_kategori_skor`.`skor`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where (`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`)) AS `skor`,(select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where (`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`)) AS `jumlah`,`pendaftar`.`status` AS `status`,`bea`.`beasiswaDibuka` AS `beasiswaDibuka`,`bea`.`beasiswaTutup` AS `beasiswaTutup`,`bea`.`periodeBerakhir` AS `periodeBerakhir`,curdate() AS `now`,(to_days(curdate()) - to_days(`bea`.`beasiswaTutup`)) AS `berakhirPendaftaran`,(to_days(curdate()) - to_days(`bea`.`periodeBerakhir`)) AS `berakhirPeriode`,`pendaftar`.`waktuDiubah` AS `updated` from ((`pendaftar` left join `bea` on((`pendaftar`.`idBea` = `bea`.`id`))) left join `identitas_mhs` on((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`))) where ((`pendaftar`.`status` = '1') and (year(`bea`.`beasiswaTutup`) between (year(curdate()) - 1) and (year(curdate()) + 1)) and (year(`bea`.`periodeBerakhir`) between (year(curdate()) - 1) and (year(curdate()) + 1))) order by (select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where (`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`)) desc ;

-- --------------------------------------------------------

--
-- Struktur untuk view `skor_mahasiswa`
--
DROP TABLE IF EXISTS `skor_mahasiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `skor_mahasiswa`  AS  select `pendaftar`.`nim` AS `nimMhs`,`identitas_mhs`.`namaLengkap` AS `namaLengkap`,`pendaftar`.`id` AS `idPendaftar`,`pendaftar`.`idBea` AS `idBeasiswa`,`bea`.`namaBeasiswa` AS `namaBeasiswa`,`pendaftar`.`ipk` AS `ipk`,(select sum(`set_sub_kategori_skor`.`skor`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where ((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`) and (`pendaftar`.`idBea` = `idBeasiswa`))) AS `skor`,(select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where ((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`) and (`pendaftar`.`idBea` = `idBeasiswa`))) AS `jumlah`,`pendaftar`.`status` AS `status`,`pendaftar`.`waktuDiubah` AS `updated` from ((`pendaftar` left join `bea` on((`pendaftar`.`idBea` = `bea`.`id`))) left join `identitas_mhs` on((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`))) order by (select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where ((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`) and (`pendaftar`.`idBea` = `idBeasiswa`))) desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_level` (`idLevel`);

--
-- Indexes for table `bea`
--
ALTER TABLE `bea`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`idBerita`);

--
-- Indexes for table `berkas_upload`
--
ALTER TABLE `berkas_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `identitas_mhs`
--
ALTER TABLE `identitas_mhs`
  ADD PRIMARY KEY (`nimMhs`),
  ADD UNIQUE KEY `nimMhs` (`nimMhs`),
  ADD KEY `jurusan-mhs` (`idJrs`),
  ADD KEY `id_akses` (`idAkses`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurusan_fk` (`idFk`);

--
-- Indexes for table `kategori_skor`
--
ALTER TABLE `kategori_skor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levellog`
--
ALTER TABLE `levellog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nim` (`nim`),
  ADD KEY `idBea` (`idBea`);

--
-- Indexes for table `pendaftar_skor`
--
ALTER TABLE `pendaftar_skor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPendaftar` (`idPendaftar`),
  ADD KEY `idBea` (`idBea`),
  ADD KEY `idKategori` (`idKategori`),
  ADD KEY `idSubKategori` (`idSubKategori`);

--
-- Indexes for table `pendaftar_upload`
--
ALTER TABLE `pendaftar_upload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPendaftar` (`idPendaftar`),
  ADD KEY `idBea` (`idBea`),
  ADD KEY `idBerkas` (`idBerkas`);

--
-- Indexes for table `pengumuman_upload`
--
ALTER TABLE `pengumuman_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil_admin`
--
ALTER TABLE `profil_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_akses` (`idAkses`);

--
-- Indexes for table `set_bea_berkas_upload`
--
ALTER TABLE `set_bea_berkas_upload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idBea` (`idBea`),
  ADD KEY `idBerkas` (`idBerkas`);

--
-- Indexes for table `set_bea_kategori_skor`
--
ALTER TABLE `set_bea_kategori_skor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_bea` (`idBea`),
  ADD KEY `id_kategori_skor` (`idKategoriSkor`);

--
-- Indexes for table `set_sub_kategori_skor`
--
ALTER TABLE `set_sub_kategori_skor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori_skor` (`idKategoriSkor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `bea`
--
ALTER TABLE `bea`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `idBerita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `berkas_upload`
--
ALTER TABLE `berkas_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `kategori_skor`
--
ALTER TABLE `kategori_skor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `levellog`
--
ALTER TABLE `levellog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pendaftar`
--
ALTER TABLE `pendaftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `pendaftar_skor`
--
ALTER TABLE `pendaftar_skor`
  MODIFY `id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `pendaftar_upload`
--
ALTER TABLE `pendaftar_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `pengumuman_upload`
--
ALTER TABLE `pengumuman_upload`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `profil_admin`
--
ALTER TABLE `profil_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `set_bea_berkas_upload`
--
ALTER TABLE `set_bea_berkas_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `set_bea_kategori_skor`
--
ALTER TABLE `set_bea_kategori_skor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `set_sub_kategori_skor`
--
ALTER TABLE `set_sub_kategori_skor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD CONSTRAINT `akses_ibfk_1` FOREIGN KEY (`idLevel`) REFERENCES `levellog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `identitas_mhs`
--
ALTER TABLE `identitas_mhs`
  ADD CONSTRAINT `identitas_mhs_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jurusan-mhs` FOREIGN KEY (`idJrs`) REFERENCES `jurusan` (`id`);

--
-- Ketidakleluasaan untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `jurusan_fk` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`);

--
-- Ketidakleluasaan untuk tabel `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD CONSTRAINT `pendaftar_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `identitas_mhs` (`nimMhs`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_ibfk_2` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pendaftar_skor`
--
ALTER TABLE `pendaftar_skor`
  ADD CONSTRAINT `pendaftar_skor_ibfk_1` FOREIGN KEY (`idPendaftar`) REFERENCES `pendaftar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_skor_ibfk_2` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_skor_ibfk_3` FOREIGN KEY (`idKategori`) REFERENCES `kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_skor_ibfk_4` FOREIGN KEY (`idSubKategori`) REFERENCES `set_sub_kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pendaftar_upload`
--
ALTER TABLE `pendaftar_upload`
  ADD CONSTRAINT `pendaftar_upload_ibfk_1` FOREIGN KEY (`idPendaftar`) REFERENCES `pendaftar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_upload_ibfk_2` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_upload_ibfk_3` FOREIGN KEY (`idBerkas`) REFERENCES `berkas_upload` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `profil_admin`
--
ALTER TABLE `profil_admin`
  ADD CONSTRAINT `profil_admin_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `set_bea_berkas_upload`
--
ALTER TABLE `set_bea_berkas_upload`
  ADD CONSTRAINT `set_bea_berkas_upload_ibfk_1` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `set_bea_berkas_upload_ibfk_2` FOREIGN KEY (`idBerkas`) REFERENCES `berkas_upload` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `set_bea_kategori_skor`
--
ALTER TABLE `set_bea_kategori_skor`
  ADD CONSTRAINT `set_bea_kategori_skor_ibfk_1` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `set_bea_kategori_skor_ibfk_2` FOREIGN KEY (`idKategoriSkor`) REFERENCES `kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `set_sub_kategori_skor`
--
ALTER TABLE `set_sub_kategori_skor`
  ADD CONSTRAINT `set_sub_kategori_skor_ibfk_1` FOREIGN KEY (`idKategoriSkor`) REFERENCES `kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
