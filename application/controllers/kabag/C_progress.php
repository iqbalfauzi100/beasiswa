<?php defined('BASEPATH')OR exit('akses di tolak');

class C_progress extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();

    $this->load->model("grafik/Grafik",'grf');
    $this->load->model("mahasiswa/StatusBea",'mod');
    $this->load->model("mahasiswa/PengumumanPenerimaBeasiswa",'model');
  }

  public function index()
  {
    $isi['tahun']     = $this->grf->get_tahun();
    $isi['beasiswa']  = $this->db->get('bea')->result();

    $this->load->view('attribute/header_kabag');
    $this->load->view('kabag/progress_beasiswa',$isi);
    $this->load->view('attribute/footer');

  }

public function datatable(){
  $tahun =$this->input->post('tahun')?$this->input->post('tahun'):0;
  $bea =$this->input->post('beasiswa')?$this->input->post('beasiswa'):0;

  $fetch_data = $this->mod->make_datatables($tahun, $bea);
  $data = array();
  $nmr = 0;

  foreach($fetch_data as $row)
  {
    $nmr +=1;
    $sub_array = array();
    $sub_array[] = $nmr;
    $sub_array[] = $row->namaBeasiswa;
    $sub_array[] = $row->penyelenggaraBea;
    $sub_array[] = $row->beasiswaDibuka;
    $sub_array[] = $row->beasiswaTutup;
    $sub_array[] = $row->periodeBerakhir;
    // $sub_array[] = '<a href="javascript:void()" onclick="progress('.$row->progress.')" class="btn blue btn"><i class="mdi-action-event"></i></a>';
    $sub_array[] = '<a href="javascript:void()" onclick="view_detail_progress('.$row->id.')" class="btn blue btn"><i class="mdi-action-event"></i></a>';
    
    $data[] = $sub_array;
  }

  $output = array(
    "draw"            =>  intval($_POST["draw"]),
    "recordsTotal"    =>  $this->mod->get_all_data(),
    "recordsFiltered" =>  $this->mod->get_filtered_data($tahun, $bea),
    "data"            =>  $data
    );
  echo json_encode($output);
    // echo "<br/>".$tahun."<br/>".$fakultas."<br/>".$jurusan."<br/>".$bea;
}

public function view_detail_progress($idBea)
{
  $data = $this->mod->get_by_id_bea($idBea);
  echo json_encode($data);
}

public function getjurusan()
{
  $fakultas = $_GET['fakultas'];
  $getjur = $this->model->get_jurusan($fakultas);
  echo json_encode($getjur); 
}
public function getbeasiswa()
{
  $tahun = $_GET['tahun'];
  $getbeasiswa = $this->model->get_beasiswa($tahun);
  echo json_encode($getbeasiswa); 
}

}
?>

