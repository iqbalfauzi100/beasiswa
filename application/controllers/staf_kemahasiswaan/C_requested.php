<?php defined('BASEPATH')OR exit('tidak ada akses diizinkan');
class C_requested extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    
    $this->load->model("staff_kemahasiswaan/Requested",'mdl');
    $this->load->model("mahasiswa/DaftarBeasiswa",'model');
    $this->load->model("mahasiswa/Formulir",'mod');
  }

  public function index()
  {
    $this->load->view('attribute/header_staff');
    $this->load->view('staff_kemahasiswaan/requested');
    $this->load->view('attribute/footer');
  }

  public function pengaturan()
  {
    $id = $this->input->post('idPengaturan');
    if ($id != null) {
      $detailBea= $this->mdl->get_by_id_bea($id);
      $data = array(
        'idSetBea' => $id,
        'nama' => $detailBea->namaBeasiswa,
        'penyelenggara' => $detailBea->penyelenggaraBea,
        'selektor' => $detailBea->selektor,
        'keterangan' => $detailBea->keterangan,
        'dibuka' => $detailBea->beasiswaDibuka,
        'ditutup' => $detailBea->beasiswaTutup,
        'periodeBerakhir' =>$detailBea->periodeBerakhir,
        'seleksiTutup' =>$detailBea->seleksiTutup,
        'kuota' => $detailBea->kuota,
        'namaFk' => $detailBea->namaFk,
        'statusTampil' => $detailBea->statusTampil,
        'skor' => $this->mdl->get_skor_by_idBea($id),
        'berkas' => $this->mdl->get_berkas_by_idBea($id),
        'combo' => $this->mdl->get_scoring(),
        'combo_berkas' => $this->mdl->get_berkas(),
        'combo_fkltas' => json_encode($this->mdl->get_fakultas())
        );
    }else {
      $data = array(
        'idSetBea' => "",
        'nama' => "",
        'penyelenggara' => "",
        'selektor' => "",
        'keterangan' => "",
        'dibuka' => "",
        'ditutup' => "",
        'periodeBerakhir'=>"",
        'seleksiTutup' =>"",
        'kuota' => "",
        'namaFk' => "",
        'statusTampil' => "",
        'skor' => null,
        'berkas' => null,
        'combo_fkltas' => json_encode($this->mdl->get_fakultas())
        );
    }
    $this->load->view('attribute/header_staff');
    $this->load->view('staff_kemahasiswaan/pengaturan', $data);
    $this->load->view('attribute/footer');
  }

  public function datatable(){
    $fetch_data = $this->mdl->make_datatables();
    $data = array();
    $nmr = 0;
    foreach($fetch_data as $row)
    {
      $nmr+=1;
      $sub_array = array();
      $sub_array[] = $nmr;
      $sub_array[] = $row->namaBeasiswa;
      $sub_array[] = $row->penyelenggaraBea;
      if ($row->selektor == "1") {
        # kasubag kemahasiswaan
        $sub_array[] = '<i class="mdi-toggle-radio-button-on" title="Kasubag. Kemahasiswaan"></i> Kms';
      }elseif ($row->selektor == "2") {
        # kasubag kemahasiswaan fakultas
        $sub_array[] = '<i class="mdi-toggle-radio-button-off" title="Kasubag. Kemahasiswaan Fakultas"></i> Fks';
      }elseif ($row->selektor == "3") {
        # keduanya
        $sub_array[] = '<i class="mdi-action-stars" title="Keduanya"></i> 2K';
      }
      $status = $row->statusBeasiswa;
      $pg = $row->progress;
      $prog;
      if ($pg==1){
        $prog = "1. Pendaftaran";
      }else if($pg==2){
        $prog = "2. Seleksi";
      }else if($pg==3){
        $prog = "3. Kelulusan";
      }else {
        $prog = "4. Pencairan";
      }

      if ($status=="0" || $status=="1" ||$status=="3") {
        # aktif/confirmed
        $sub_array[] = '<span class="success-text">Telah Dikonfirmasi</span>';
        $sub_array[] = '<a href="javascript:void()" onclick="edit('.$row->id.')">'.$prog.'</a>';
        $sub_array[] = '
        <a class="btn-floating waves-effect waves-light primary-color z-depth-0" title="Confirmed"><i class="mdi-action-done"></i></a>
        ';
      }else{
        # ditolak
        $sub_array[] = '<span class="alert-text">'.$row->keterangan.'</span>';
        $sub_array[] = '<a href="javascript:void()" onclick="edit('.$row->id.')">'.$prog.'</a>';
        $alamat = base_url('staf_kemahasiswaan/C_requested/pengaturan');
        $sub_array[] = '
        <form action="'.$alamat.'" method="post">
          <button class="btn-floating waves-effect waves-light red" title="Not Confirmed" type="submit" name="idPengaturan" value="'.$row->id.'"><i class="mdi-action-settings"></i></button>
        </form>
        ';
      }

      $url = null;
      if ($row->namaBeasiswa == "Beasiswa Putra Daerah"){
        $url = base_url('staf_kemahasiswaan/C_requested/addDataDaerah');
      }else{
        $url = base_url('staf_kemahasiswaan/C_requested/addData');
      }

      if($status=="3"){
        $sub_array[] = '
        <form action="'.$url.'" method="post">
          <button class="btn-floating waves-effect waves-light blue" title="Tambah data Mahasiswa" type="submit" name="idPengaturan" value="'.$row->id.'"><i class="mdi-content-add"></i></button>
        </form>
        ';
      }else{
        $sub_array[] = '
        <button onclick="status()" class="btn-floating waves-effect waves-light blue" title="Beasiswa belum di ACC Kabag"><i class="mdi-content-add"></i></button>
        ';
      }
      $data[] = $sub_array;
    }
    $output = array(
      "draw"            =>  intval($_POST["draw"]),
      "recordsTotal"    =>  $this->mdl->get_all_data(),
      "recordsFiltered" =>  $this->mdl->get_filtered_data(),
      "data"            =>  $data
      );
    echo json_encode($output);
  }

  public function addData()
  {
    $id = $this->input->post('idPengaturan');
    $data = array(
      'idBea'    => $id,
      'namaBea'  => $this->mod->get_nama_bea($id),
      'combo'    => $this->mod->get_skor_bea($id),
      'berkas'   => $this->mod->get_berkas_upload($id),
      'jurusan'  => $this->mod->get_jurusan(),
      'dataJur'  => $this->model->getJurusan(),
      'fakultas' => $this->mod->dataFakultas()
      );

    $this->load->view('staff_kemahasiswaan/formulir', $data);
  }
  public function addDataDaerah()
  {
    $id = $this->input->post('idPengaturan');
    $detailBea = $this->mdl->get_by_id_bea($id);
    $data = array(
      'idBea'    => $id,
      'penyelenggaraBea' => $detailBea->penyelenggaraBea,
      'namaBea'  => $this->mod->get_nama_bea($id),
      'jurusan'  => $this->mod->get_jurusan(),
      'dataJur'  => $this->model->getJurusan(),
      'fakultas' => $this->mod->dataFakultas()
      );

    $this->load->view('staff_kemahasiswaan/formulirDaerah', $data);
  }

  public function ajax_edit($id)
  {
    $data = $this->mdl->get_by_id($id);
    echo json_encode($data);
  }

  public function get_scoring_data()
  {
    $data = $this->mdl->get_scoring();
    echo json_encode($data);
  }
  public function get_berkas_data()
  {
    $data_berkas = $this->mdl->get_berkas();
    echo json_encode($data_berkas);
  }

  public function add_data()
  {
    $slktr = $this->input->post('selektor');
    if($slktr == "2"){
      $data_bea = array(
        'namaBeasiswa' => $this->input->post('nama'),
        'penyelenggaraBea' => $this->input->post('penyelenggara'),
        'beasiswaDibuka' => $this->input->post('dibuka'),
        'beasiswaTutup' => $this->input->post('ditutup'),
        'periodeBerakhir' => $this->input->post('periodeBerakhir'),
        'seleksiTutup' => $this->input->post('penyeleksianDitutup'),
        'kuota' => $this->input->post('kuota'),
        'statusTampil' => $this->input->post('statusTampil'),
        'selektor' => $this->input->post('selektor'),
        'selektorFakultas' => $this->input->post('selektor_fakultas')
        );
    }else{
      $data_bea = array(
        'namaBeasiswa' => $this->input->post('nama'),
        'penyelenggaraBea' => $this->input->post('penyelenggara'),
        'beasiswaDibuka' => $this->input->post('dibuka'),
        'beasiswaTutup' => $this->input->post('ditutup'),
        'periodeBerakhir' => $this->input->post('periodeBerakhir'),
        'seleksiTutup' => $this->input->post('penyeleksianDitutup'),
        'kuota' => $this->input->post('kuota'),
        'statusTampil' => $this->input->post('statusTampil'),
        'selektor' => $this->input->post('selektor')
        );
    }

    $insert_bea = $this->mdl->save_bea($data_bea);

    $count_score = count($this->input->post('score'));
    if ($this->input->post('score')) {
      $data = array();
      for ($i=0;$i<$count_score;$i++) {
        $skor = $this->input->post('score['.$i.']');
        if ($skor != "HAPUS") {
          $data[]= array(
            'idBea' => $insert_bea,
            'idKategoriSkor' => $skor,
            );
        }
      }
      $insert_sub_bea = $this->mdl->save_sub_bea($data);
    }

    $count_berkas = count($this->input->post('berkas'));
    if ($this->input->post('berkas')) {
      $data_berkas = array();
      for ($i=0;$i<$count_berkas;$i++) {
        $berkas = $this->input->post('berkas['.$i.']');
        if ($berkas != "HAPUS") {
          $data_berkas[]= array(
            'idBea' => $insert_bea,
            'idBerkas' => $berkas,
            );
        }
      }
      $insert_sub_bea_berkas = $this->mdl->save_sub_bea_berkas($data_berkas);
    }
    echo json_encode(array("status" => TRUE));
  }

  public function update_data()
  {
    $slktr = $this->input->post('selektor');
    if($slktr == "2"){
      $data_bea = array(
        'namaBeasiswa' => $this->input->post('nama'),
        'penyelenggaraBea' => $this->input->post('penyelenggara'),
        'beasiswaDibuka' => $this->input->post('dibuka'),
        'beasiswaTutup' => $this->input->post('ditutup'),
        'periodeBerakhir' => $this->input->post('periodeBerakhir'),
        'seleksiTutup' => $this->input->post('penyeleksianDitutup'),
        'kuota' => $this->input->post('kuota'),
        'statusTampil' => $this->input->post('statusTampil'),
        'selektor' => $this->input->post('selektor'),
        'selektorFakultas' => $this->input->post('selektor_fakultas')
        );
    }else {
      $data_bea = array(
        'namaBeasiswa' => $this->input->post('nama'),
        'penyelenggaraBea' => $this->input->post('penyelenggara'),
        'beasiswaDibuka' => $this->input->post('dibuka'),
        'beasiswaTutup' => $this->input->post('ditutup'),
        'periodeBerakhir' => $this->input->post('periodeBerakhir'),
        'seleksiTutup' => $this->input->post('penyeleksianDitutup'),
        'kuota' => $this->input->post('kuota'),
        'statusTampil' => $this->input->post('statusTampil'),
        'selektor' => $this->input->post('selektor'),
        'selektorFakultas' => null
        );
    }
    $idSetBea = $this->input->post('idSetBea');
    $this->mdl->update_setting_bea(array('id' => $idSetBea), $data_bea);

    //Scoring Beasiswa
    $count_score = count($this->input->post('score'));
    for ($i=0;$i<$count_score;$i++) {
      $skor = $this->input->post('score['.$i.']');
      $idSet = $this->input->post('idSet['.$i.']');
      if ($skor != null && $idSet == null && $skor != "HAPUS") {
        # insert kategori
        $data= array(
          'idBea' => $idSetBea,
          'idKategoriSkor' => $skor,
          );
        $this->mdl->insert_setting_sub_bea($data);
      }elseif ($skor != null && $idSet != null && $skor != "HAPUS") {
        # update kategori
        $data= array(
          'idBea' => $idSetBea,
          'idKategoriSkor' => $skor,
          );
        $this->mdl->update_setting_sub_bea(array('id' => $idSet), $data);
      }elseif ($skor != null && $idSet != null && $skor == "HAPUS") {
        # delete kategori
        $this->mdl->delete_setting_sub_bea($idSet);
      }
    }

    //Berkas Upload Beasiswa
    $count_berkas = count($this->input->post('berkas'));
    for ($i=0;$i<$count_berkas;$i++) {
      $berkas = $this->input->post('berkas['.$i.']');
      $idSetBerkas = $this->input->post('idSetBerkas['.$i.']');
      if ($berkas != null && $idSetBerkas == null && $berkas != "HAPUS") {
        # insert kategori
        $data_berkas= array(
          'idBea' => $idSetBea,
          'idBerkas' => $berkas,
          );
        $this->mdl->insert_setting_sub_bea_berkas($data_berkas);
      }elseif ($berkas != null && $idSetBerkas != null && $berkas != "HAPUS") {
        # update kategori
        $data_berkas= array(
          'idBea' => $idSetBea,
          'idBerkas' => $berkas,
          );
        $this->mdl->update_setting_sub_bea_berkas(array('id' => $idSetBerkas), $data_berkas);
      }elseif ($berkas != null && $idSetBerkas != null && $berkas == "HAPUS") {
        # delete kategori
        $this->mdl->delete_setting_sub_bea_berkas($idSetBerkas);
      }
    }
    echo json_encode(array("status" => TRUE));
  }

  public function update_progress()
  {
    $data_bea = array(
      'progress' => $this->input->post('progress'),
      'colorProgress' => $this->input->post('colorProgress')
      );
    $idSetBea = $this->input->post('idBea');
    $this->mdl->update_setting_bea(array('id' => $idSetBea), $data_bea);
    // echo json_encode($idSetBea);
    // echo json_encode($data_bea);
    echo json_encode(array("status" => TRUE));
  }

  public function delete_data()
  {
    $id = $this->input->post('idSetBea');
    $this->mdl->delete_by_id($id);
    echo json_encode(array("status" => TRUE));
  }

}
?>
