<?php defined('BASEPATH')OR exit('tidak ada akses diizinkan');
class C_upload_transfer extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    
    $this->load->model('kasubag/Beasiswa');
    $this->load->model('kasubag/ReportBeasiswa');
    $this->load->model("grafik/Grafik",'grf');
    $this->load->model("kasubag/Upload_bukti_transfer",'mdl');
    $this->load->model("mahasiswa/PengumumanPenerimaBeasiswa",'model');

    $this->load->library('upload');
    $this->load->helper(array('url'));
  }

  public function index()
  {
    $data['fakultas'] = $this->ReportBeasiswa->dataFakultas();
    $data['beasiswa'] = $this->Beasiswa->daftarBeasiswa();
    $data['tahun']    = $this->grf->get_tahun();

    $this->load->view('attribute/header_staff');
    $this->load->view('staff_kemahasiswaan/buktiTransfer',$data);
    $this->load->view('attribute/footer');
  }
  public function getbeasiswa()
  {
    $tahun = $_GET['tahun'];
    $getbeasiswa = $this->model->get_beasiswa($tahun);
    echo json_encode($getbeasiswa); 
  }
  public function getJurusan() {
    $fakultas = $_GET['fakultas'];
    $getjur = $this->ReportBeasiswa->get_jurusan($fakultas);
    echo json_encode($getjur);
  }

  public function get_data_print($tahun, $fakultas,$jurusan,$bea, $jenisKel)
  {
    $data['databea'] = $this->mdl->master_print($tahun, $fakultas, $jurusan, $bea, $jenisKel);
    $this->load->view('kasubag/masterDataBuktiTransfer', $data);
  }
  public function get_print_excell_transfer($tahun, $fakultas,$jurusan,$bea, $jenisKel)
  {

    $data['databea'] = $this->mdl->master_print($tahun, $fakultas, $jurusan, $bea,$jenisKel);
    $this->load->view('kasubag/masterDataBuktiTransferExcell', $data);
  }

  public function datatable(){
    $tahun    = $this->input->post('tahun')?$this->input->post('tahun'):0;
    $fakultas = $this->input->post('fakultas')?$this->input->post('fakultas'):0;
    $jurusan  = $this->input->post('jurusan')?$this->input->post('jurusan'):0;
    $bea      = $this->input->post('beasiswa')?$this->input->post('beasiswa'):0;
    $jenisKel = $this->input->post('jenisKel')?$this->input->post('jenisKel'):0;

    $fetch_data = $this->mdl->make_datatables($tahun, $fakultas, $jurusan, $bea, $jenisKel);
    $data = array();
    $nmr = 0;
    foreach($fetch_data as $row)
    {
      $nmr+=1;
      $sub_array = array();
      $sub_array[] = $nmr;
      $sub_array[] = $row->nim;
      $sub_array[] = $row->namaLengkap;
      $sub_array[] = $row->namaFk;
      $sub_array[] = $row->namaJur;
      $sub_array[] = $row->namaBeasiswa;
      $sub_array[] = $row->penyelenggaraBea;
      $sub_array[] = $row->tanggal;
      $sub_array[] = '
      <b>File : </b><i><a href="'.base_url().'assets/img/upload_bukti_transfer/'.$row->file.'" target="_blank">Berkas</a>
      ';

      $sub_array[] = $row->tahun;
      
      $sub_array[] = '
      <button type="button" name="remove" id="'.$row->id.'" onclick="remove('."'".$row->id."','".$row->nim."'".')" class="btn-floating waves-effect waves-light red" title="Hapus"><i class="mdi-action-delete">delete</i></button>
      ';
      $data[] = $sub_array;
    }
    $output = array(
      "draw"            =>  intval($_POST["draw"]),
      "recordsTotal"    =>  $this->mdl->get_all_data(),
      "recordsFiltered" =>  $this->mdl->get_filtered_data($tahun, $fakultas, $jurusan, $bea, $jenisKel),
      "data"            =>  $data
      );
    echo json_encode($output);
  }

  

  public function edit_data($id)
  {
    $pengumuman = $this->mdl->get_by_id($id);
    $id = $pengumuman->id;
    $tanggal = $pengumuman->tanggal;
    $file = $pengumuman->file;

    $upload = $this->mdl->get_by_id_upload_berkas($id);
    $data = array();
    $data [0] = array(
      'id' => $id,
      'tanggal' => $tanggal,
      'file' => $file
      );
    echo json_encode($data);
  }

  public function update_data()
  {
    $config['upload_path']="./assets/img/upload_bukti_transfer";
    $config['allowed_types']='gif|jpg|png|jpeg|pdf|rar|zip|docx|xlsx|pptx|ppt|xls|doc';
    $config['max_size'] = '1000';

    $this->upload->initialize($config);
    

    if($_FILES['file']['name'])
    {
      if ($this->upload->do_upload('file'))
      {
        $file = $this->upload->data();
        $data = array(
          'tahun'   => $this->input->post('tahunn'),
          'idBea'   => $this->input->post('beasiswaa'),
          'tanggal' => $this->input->post('tanggal'),
          'file'    => $file['file_name']
          );

        $id = $this->input->post('idUpload');
        $change = $this->mdl->get_by_id($id);
        unlink('assets/img/upload_bukti_transfer/'.$change->file);

        $where =array('id'=>$id);
        $this->mdl->get_update($data,$where);
        
        $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil di Update</div>");
        redirect('staf_kemahasiswaan/C_upload_transfer');
      }
      else
      {
        $this->session->set_flashdata("pesan", "<div class=\"card-panel alert\">Gagal Update [max size 1 Mb] [types : jpg, png, jpeg, gif, pdf, docx, xlsx, pptx, rar, zip]</div>");
        redirect('staf_kemahasiswaan/C_upload_transfer');
      }
    }else if(empty($_FILES['file']['name']))
    {
      $data = array(
        'tahun'   => $this->input->post('tahunn'),
        'idBea'   => $this->input->post('beasiswaa'),
        'tanggal' => $this->input->post('tanggal')
        );
      $id = $this->input->post('idUpload');
      $this->mdl->getupdate($id,$data);
      $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil di Update</div>");
      redirect('staf_kemahasiswaan/C_upload_transfer');
    }

  }

  public function delete_data($id)
  {
    $delete = $this->mdl->get_by_id($id);
    if(file_exists('assets/img/upload_bukti_transfer/'.$delete->file) && $delete->file)
      unlink('assets/img/upload_bukti_transfer/'.$delete->file);
    
    $this->mdl->delete_by_id($id);
    echo json_encode(array("status" => TRUE));
  }
}
?>