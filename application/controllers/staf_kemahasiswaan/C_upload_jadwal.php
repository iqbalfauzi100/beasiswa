<?php defined('BASEPATH')OR exit('tidak ada akses diizinkan');
class C_upload_jadwal extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    
    $this->load->model("staff_kemahasiswaan/Jadwal_upload",'mdl');

    $this->load->library('upload');
    $this->load->helper(array('url'));
  }

  public function index()
  {
    $this->load->view('attribute/header_staff');
    $this->load->view('staff_kemahasiswaan/upload_jadwal');
    $this->load->view('attribute/footer');
  }

  public function datatable(){
    $fetch_data = $this->mdl->make_datatables();
    $data = array();
    $nmr = 0;
    foreach($fetch_data as $row)
    {
      $nmr+=1;
      $sub_array = array();
      $sub_array[] = $nmr;
      $sub_array[] = $row->judul;
      $sub_array[] = $row->tanggal;
      $sub_array[] = '
      <b>File : </b><i><a href="'.base_url().'assets/img/upload_pengumuman/'.$row->file.'" target="_blank">'.$row->file.'</a>
      ';
      $sub_array[] = '
      <button type="button" name="edit" id="'.$row->id.'" onclick="edit('."'".$row->id."'".')" class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="mdi-editor-mode-edit"></i></button>
      <button type="button" name="remove" id="'.$row->id.'" onclick="remove('."'".$row->id."','".$row->judul."'".')" class="btn-floating waves-effect waves-light red" title="Hapus"><i class="mdi-action-delete">delete</i></button>
      ';
      $data[] = $sub_array;
    }
    $output = array(
      "draw"            =>  intval($_POST["draw"]),
      "recordsTotal"    =>  $this->mdl->get_all_data(),
      "recordsFiltered" =>  $this->mdl->get_filtered_data(),
      "data"            =>  $data
      );
    echo json_encode($output);
  }

  public function save_data()
  {
    $config['upload_path']="./assets/img/upload_pengumuman";
    $config['allowed_types']='gif|jpg|png|jpeg|pdf|rar|zip|docx|xlsx|pptx|ppt|xls|doc';
    $config['max_size'] = '1000';

    $this->upload->initialize($config);

    if(isset($_FILES['file']['name']))
    {
      if ($this->upload->do_upload('file'))
      {
        $file = $this->upload->data();
        $data = array(
          'judul'   => $this->input->post('judul'),
          'tanggal' => $this->input->post('tanggal'),
          'file'    => $file['file_name']
          );
        $insert = $this->mdl->save_berkas($data);
        $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil Disimpan</div>");
        redirect('staf_kemahasiswaan/C_upload_jadwal');
      }
      else
      {
        $this->session->set_flashdata("pesan", "<div class=\"card-panel alert\">Gagal Upload [max size 1 Mb] [types : jpg, png, jpeg, gif, pdf, docx, xlsx, pptx, rar, zip]</div>");
        redirect('staf_kemahasiswaan/C_upload_jadwal');
      }
    }else if(empty($_FILES['file']['name']))
    {
      $data = array(
        'judul'   => $this->input->post('judul'),
        'tanggal' => $this->input->post('tanggal')
        );
      $insert = $this->mdl->save_berkas($data);
      $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil Disimpan</div>");
      redirect('staf_kemahasiswaan/C_upload_jadwal');
    }
    
  }

  public function edit_data($id)
  {
    $pengumuman = $this->mdl->get_by_id($id);
    $id = $pengumuman->id;
    $judul = $pengumuman->judul;
    $tanggal = $pengumuman->tanggal;
    $file = $pengumuman->file;

    $upload = $this->mdl->get_by_id_upload_berkas($id);
    $data = array();
    $data [0] = array(
      'id' => $id,
      'judul' => $judul,
      'tanggal' => $tanggal,
      'file' => $file
      );
    echo json_encode($data);
  }

  public function update_data()
  {
    $config['upload_path']="./assets/img/upload_pengumuman";
    $config['allowed_types']='gif|jpg|png|jpeg|pdf|rar|zip|docx|xlsx|pptx|ppt|xls|doc';
    $config['max_size'] = '1000';

    $this->upload->initialize($config);
    

    if($_FILES['file']['name'])
    {
      if ($this->upload->do_upload('file'))
      {
        $file = $this->upload->data();
        $data = array(
          'judul'   => $this->input->post('judul'),
          'tanggal' => $this->input->post('tanggal'),
          'file'    => $file['file_name']
          );

        $id = $this->input->post('idPengumuman');
        $change = $this->mdl->get_by_id($id);
        unlink('assets/img/upload_pengumuman/'.$change->file);

        $where =array('id'=>$id);
        $this->mdl->get_update($data,$where);
        
        $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil di Update</div>");
        redirect('staf_kemahasiswaan/C_upload_jadwal');
      }
      else
      {
        $this->session->set_flashdata("pesan", "<div class=\"card-panel alert\">Gagal Update [max size 1 Mb] [types : jpg, png, jpeg, gif, pdf, docx, xlsx, pptx, rar, zip]</div>");
        redirect('staf_kemahasiswaan/C_upload_jadwal');
      }
    }else if(empty($_FILES['file']['name']))
    {
      $data = array(
        'judul'   => $this->input->post('judul')
        );
      $id = $this->input->post('idPengumuman');
      $this->mdl->getupdate($id,$data);
      $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil di Update</div>");
      redirect('staf_kemahasiswaan/C_upload_jadwal');
    }

  }

  public function delete_data($id)
  {
    $delete = $this->mdl->get_by_id($id);
    if(file_exists('assets/img/upload_pengumuman/'.$delete->file) && $delete->file)
      unlink('assets/img/upload_pengumuman/'.$delete->file);
    
    $this->mdl->delete_by_id($id);
    echo json_encode(array("status" => TRUE));
  }
}
?>