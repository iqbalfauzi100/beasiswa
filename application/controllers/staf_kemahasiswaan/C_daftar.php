<?php defined('BASEPATH')OR exit('akses tidak dapat diterima');

class C_daftar extends CI_Controller
{

  function __construct(){
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    $this->load->model("mahasiswa/Formulir",'mdl');

    $this->load->library('upload');
    $this->load->helper(array('url'));
  }

  public function index()
  {
    $nim = $this->session->userdata('username');
    $idBea = $this->input->post('idPengaturan');

    $dataMhs = $this->mdl->getdataMhs($nim);
    $data = array(
      'idBea' => $idBea,
      'namaBea' => $this->mdl->get_nama_bea($idBea),
      'combo' => $this->mdl->get_skor_bea($idBea),
      'jurusan' => $this->mdl->get_jurusan(),
      'nim' => $dataMhs->nimMhs,
      'nama' => $dataMhs->namaLengkap,
      'tempatLahir' => $dataMhs->tempatLahir,
      'tglLahir' => $dataMhs->tglLahir,
      'asalKota' => $dataMhs->asalKota,
      'noTelp' => $dataMhs->noTelp
      );
    $this->load->view('mahasiswa/formulir', $data);
  }
  
  public function simpan()
  {
    $nim  = $this->input->post('nim');
    $data =array(
      'idBea'     => $this->input->post('idBea'),
      'nim'       => $this->input->post('nim'),
      'semester'  => $this->input->post('semester'),
      'sks'       => $this->input->post('sks'),
      'ipk'       => $this->input->post('ipk'),
      'alamatMalang' => $this->input->post('alamatMalang'),
      'tanggal'   => $this->input->post('tanggal')
      );

    $pendaftar = $this->mdl->getInsert($data);

      // echo(json_encode($this->input->post('idKategoriSkor')));
      // echo(json_encode($this->input->post('score')));

    $count_score = count($this->input->post('idKategoriSkor'));
    $data1 = array();
    for ($i=0;$i<$count_score;$i++) {
      $kategori_skor = $this->input->post('idKategoriSkor['.$i.']');
      $skor          = $this->input->post('score['.$i.']');
      $data1[]= array(
        'idKategori'    => $kategori_skor,
        'idSubKategori' => $skor,
        'idBea'         => $this->input->post('idBea'),
        'idPendaftar'   => $pendaftar
        );
    }

    $this->mdl->pendaftarSkor($data1);

    if($this->input->post('action1') && !empty($_FILES['fileBerkas']['name'])){
      $filesCount = count($_FILES['fileBerkas']['name']);
      for($i = 0; $i < $filesCount; $i++){
        $_FILES['fileberkas']['name'] = $_FILES['fileBerkas']['name'][$i];
        $_FILES['fileberkas']['type'] = $_FILES['fileBerkas']['type'][$i];
        $_FILES['fileberkas']['tmp_name'] = $_FILES['fileBerkas']['tmp_name'][$i];
        $_FILES['fileberkas']['error'] = $_FILES['fileBerkas']['error'][$i];
        $_FILES['fileberkas']['size'] = $_FILES['fileBerkas']['size'][$i];

        $uploadPath = 'assets/img/upload_berkas/';
        $config['upload_path'] = $uploadPath;
          // $config['allowed_types'] = '*';
        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|docx|doc';
        $config['file_name'] = "file_".time();

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('fileberkas')){
          $fileData = $this->upload->data();
          $uploadData[$i]['title'] = $fileData['file_name'];
          $uploadData[$i]['type'] = $fileData['file_type'];
          $uploadData[$i]['ukuran'] = $fileData['file_size'];
          $uploadData[$i]['created'] = date("Y-m-d H:i:s");
          $uploadData[$i]['idBerkas'] = $this->input->post('idBerkas['.$i.']');
          $uploadData[$i]['idBea'] = $this->input->post('idBea');
          $uploadData[$i]['idPendaftar'] = $pendaftar;
        }
      }
    }
    $this->mdl->pendaftarUpload($uploadData);
    $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil Disimpan</div>");
    redirect('staf_kemahasiswaan/C_requested');
  }

  public function simpanDaerah()
  {
    $nim               = $this->input->post('nim');
    $penyelenggaraBea  = $this->input->post('penyelenggaraBea');
    $data =array(
      'idBea'     => $this->input->post('idBea'),
      'nim'       => $this->input->post('nim'),
      'semester'  => $this->input->post('semester'),
      'sks'       => $this->input->post('sks'),
      'ipk'       => $this->input->post('ipk'),
      'alamatMalang' => $this->input->post('alamatMalang'),
      'tanggal'   => $this->input->post('tanggal')
      );

    $pendaftar = $this->mdl->getInsert($data);

    $this->session->set_flashdata("pesan", "<div class=\"card-panel success col s12 m4 l6\">Data Berhasil Disimpan (Beasiswa Putra Daerah - ".$penyelenggaraBea.")</div>");
    redirect('staf_kemahasiswaan/C_requested');
  }
}