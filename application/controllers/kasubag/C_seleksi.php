<?php defined('BASEPATH')OR exit('tidak ada akses diizinkan');
class C_seleksi extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    $this->load->model("mahasiswa/Formulir", 'm_aplikasi');
    $this->load->model("kasubag/Seleksi",'mdl');
    $this->load->model('kasubag/ReportBeasiswa');
  }

  public function index()
  {
    $this->load->view('attribute/header_kasubag');
    $data['comboBea'] = $this->mdl->getComboBea();
    $data['comboFakultas'] = $this->mdl->getComboFak();
    $data['comboAngkatan'] = $this->mdl->getComboAngkatan();

    $this->load->view('kasubag/seleksi',$data);
    $this->load->view('attribute/footer');
  }
  public function dataDaerah($id)
  {
    $data['pendaftar'] = $this->m_aplikasi->data_pendaftar($id);
    $data['bead'] = $this->m_aplikasi->data_penyelenggara($id);
    $this->load->view('mahasiswa/buktiDaerah',$data);
    unset($data);
  }
  public function data_pendaftar($id)
  {
   $data['pendaftar'] = $this->m_aplikasi->data_pendaftar($id);
   $data['kategori'] = $this->m_aplikasi->data_kategori($id);
   // echo json_encode($data['pendaftar']);
   $this->load->view('mahasiswa/buktipendaftaran',$data);
   unset($data);
 }
 public function datatable(){

  $idBea = $this->input->post('idBea')?$this->input->post('idBea'):0;
  $idFak = $this->input->post('idFak')?$this->input->post('idFak'):0;
  $idJur = $this->input->post('idJur')?$this->input->post('idJur'):0;
  $idAngkatan = $this->input->post('idAngkatan')?$this->input->post('idAngkatan'):0;

  $fetch_data = $this->mdl->make_datatables($idBea,$idFak,$idJur,$idAngkatan);
  $data = array();
  $nmr = 0;
  $alamat = null;
  foreach($fetch_data as $row)
  {
    $nmr+=1;
    if ($row->namaBeasiswa == "Beasiswa Putra Daerah"){
     $alamat2 = base_url('kasubag/C_seleksi/dataDaerah/'.$row->idPendaftar);
   }else{
    $alamat2 = base_url('kasubag/C_seleksi/data_pendaftar/'.$row->idPendaftar);
  }
  $sub_array = array();
  $sub_array[] = $nmr;
  $sub_array[] = $row->nimMhs;
  $sub_array[] = $row->namaLengkap;
  $sub_array[] = $row->ipk;
  $sub_array[] = '<a href="#" onclick="view_detail_score('.$row->idPendaftar.','.$row->idBeasiswa.')">'.$row->skor.'</a>';
  $sub_array[] = number_format($row->jumlah,2);
  $sub_array[] = '<a href="#" onclick="view_detail_upload('.$row->idPendaftar.','.$row->idBeasiswa.')">Berkas</a>';
  $sub_array[] = $row->updated;
  $sub_array[] = '<a href="'.$alamat2.'" class="btn-floating waves-effect waves-light primary-color z-depth-0" title="Daftar"><i class="mdi-action-print"></i></a>';
  if ($row->status==1) {
    $sub_array[] = '
    <button class="btn-floating waves-effect waves-light primary-color" title="Confirmed" type="submit" name="idPengaturan" onclick="seleksi('."'".$row->idPendaftar."'".','."'".$row->status."','".$row->nimMhs."'".');"><i class="mdi-action-done"></i></button>
    ';
  }elseif ($row->status==0) {
    $sub_array[] = '
    <button class="btn-floating waves-effect waves-light red" title="Not Confirmed" type="submit" name="idPengaturan" onclick="seleksi('."'".$row->idPendaftar."'".','."'".$row->status."','".$row->nimMhs."'".');"><i class="mdi-content-clear"></i></button>
    ';
  }
  $data[] = $sub_array;
}
$output = array(
  "draw"            =>  intval($_POST["draw"]),
  "recordsTotal"    =>  $nmr,
  "recordsFiltered" =>  $this->mdl->get_filtered_data($idBea,$idFak,$idJur,$idAngkatan),
  "data"            =>  $data
  );
echo json_encode($output);
}

public function view_detail_score($idPendaftar, $idBea)
{
  $data = $this->mdl->view_detail_score($idPendaftar, $idBea);
  echo json_encode($data);
}
public function view_detail_upload($idPendaftar, $idBea)
{
  $data = $this->mdl->view_detail_upload($idPendaftar, $idBea);
  echo json_encode($data);
}

public function getDiterima($idBea)
{
  $data = $this->mdl->infoDiterima($idBea);
  echo json_encode($data);
}
public function getJurusan() {
  $fakultas = $_GET['fakultas'];
  $getjur = $this->ReportBeasiswa->get_jurusan($fakultas);
  echo json_encode($getjur);
}

public function seleksi($idPendaftar, $status, $nim)
{
  $change_status;
  if ($status=="1") {
    $change_status = "0";
    $data = array(
      'status' => $change_status
      );
    $this->mdl->seleksi_penerima(array('id' => $idPendaftar), $data);
    echo json_encode(array("status" => TRUE));
  }elseif ($status=="0") {
    $check = $this->mdl->check_status_penerima($nim);
    if ($check==null) {
      $change_status = "1";
      $data = array(
        'status' => $change_status
        );
      $this->mdl->seleksi_penerima(array('id' => $idPendaftar), $data);
      echo json_encode(array("status" => TRUE));
        // echo "belum diterima di beasiswa";
    }else{
      $detail_diterima = array(
        'status' => FALSE,
        'nim' => $check->nim,
        'bea' => $check->namaBeasiswa,
        'periode_berakhir' => $check->periodeBerakhir
        );
      echo json_encode($detail_diterima);
        // echo "telah di terima di beasiswa";
    }
  }
}

}
?>
