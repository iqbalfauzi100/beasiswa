<?php defined('BASEPATH')OR exit('tidak ada akses diizinkan');
class C_email extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    $this->load->model("kasubag/Requested",'mdl');
    $this->load->model('kasubag/ReportBeasiswa');
    $this->load->model('kasubag/KirimEmail');
    $this->load->model("kasubag/ProfileKasubag",'profile');

    $this->load->model("grafik/Grafik",'grf');
  }

  public function index()
  {
    $data['tahun']    = $this->grf->get_tahun();
    $data['fakultas'] = $this->ReportBeasiswa->dataFakultas();

    $this->load->view('attribute/header_kasubag');
    $this->load->view('kasubag/kirimEmail',$data);
    $this->load->view('attribute/footer');
  }

  public function config_email()
  {
    $user = $this->session->userdata('id');
    $cek = $this->profile->getdata($user);
    if($this->profile->getIdentitasAdmin($user) != 0){
      $smtp_user  = $cek->email;
      $smtp_pass  = $cek->passEmail;
      $name       = $cek->nama;
    }else{
      $smtp_user  = "";
      $smtp_pass  = "";
      $name       = "";
    }
    echo json_encode($data);
  }

  public function sending_email($email,$message,$subject)
  {
    //Konfigurasi
    $user = $this->session->userdata('id');
    $cek = $this->profile->getdata($user);
    if($this->profile->getIdentitasAdmin($user) != 0){
      $smtp_user  = $cek->email;
      $smtp_pass  = $cek->passEmail;
      $name       = $cek->nama;
    }else{
      $smtp_user  = "";
      $smtp_pass  = "";
      $name       = "";
    }

  // Konfigurasi email.
    $config = [
    'useragent' => 'CodeIgniter',
    'protocol'  => 'smtp',
    'mailpath'  => '/usr/sbin/sendmail',
    'smtp_host' => 'ssl://smtp.gmail.com',
  'smtp_user' => $smtp_user,   // Ganti dengan email gmail Anda.
  'smtp_pass' => $smtp_pass,   // Password gmail Anda.
  'smtp_port' => 465,
  'smtp_keepalive' => TRUE,
  'smtp_crypto' => 'SSL',
  'wordwrap'  => TRUE,
  'wrapchars' => 80,
  'mailtype'  => 'html',
  'charset'   => 'utf-8',
  'validate'  => TRUE,
  'crlf'      => "\r\n",
  'newline'   => "\r\n",
  ];

  // Load library email dan konfigurasinya.
  $this->load->library('email', $config);

  // Pengirim dan penerima email.
  $this->email->from($smtp_user, $name); // Email dan nama pegirim.
  $this->email->to($email);      // Penerima email.

  // Lampiran email. Isi dengan url/path file.
  // $this->email->attach('https://masrud.com/themes/masrud/img/logo.png');

  // Subject email.
  $this->email->subject($subject);

  // Isi email. Bisa dengan format html.
  $this->email->message($message);

  if ($this->email->send())
  {
    echo 'Sukses! email berhasil dikirim.';
  }
  else
  {
    echo 'Error! email tidak dapat dikirim.';
  }
}

public function send_email()
{
  $email   = $this->input->post('email');
  $message = $this->input->post('message');
  $subject = $this->input->post('subject');

  $count = count($email);

  for ($i=0; $i < $count; $i++) { 
    $this->sending_email($email[$i],$message,$subject);
  }
  $this->session->set_flashdata("pesan", "<div class=\"card-panel success\">Email Berhasil Di Kirim</div>");
  redirect('kasubag/C_email');
}

public function datatable(){
  $tahun    = $this->input->post('tahun')?$this->input->post('tahun'):0;
  $fakultas = $this->input->post('fakultas')?$this->input->post('fakultas'):0;
  $jurusan  = $this->input->post('jurusan')?$this->input->post('jurusan'):0;
  $bea      = $this->input->post('beasiswa')?$this->input->post('beasiswa'):0;
  $jenisKel = $this->input->post('jenisKel')?$this->input->post('jenisKel'):0;

  $fetch_data = $this->KirimEmail->make_datatables($tahun, $fakultas, $jurusan, $bea, $jenisKel);
  $data = array();
  $nmr = 0;
  foreach($fetch_data as $row)
  {
    $nmr+=1;
    $sub_array = array();
    $sub_array[] = $nmr.'<input type="hidden" name="email[]" value="'.$row->emailAktif.'">';
    $sub_array[] = $row->nim;
    $sub_array[] = $row->namaLengkap;
    $sub_array[] = $row->namaFk;
    $sub_array[] = $row->namaJur;
    $sub_array[] = $row->penyelenggaraBea;
    $sub_array[] = $row->emailAktif;
    $sub_array[] = $row->tahun;

    $data[] = $sub_array;
  }
  $output = array(
    "draw"            =>  intval($_POST["draw"]),
    "recordsTotal"    =>  $this->KirimEmail->get_all_data(),
    "recordsFiltered" =>  $this->KirimEmail->get_filtered_data($tahun, $fakultas, $jurusan, $bea, $jenisKel),
    "data"            =>  $data
    );
  echo json_encode($output);
}

}
?>
