<?php defined('BASEPATH')OR exit('tidak ada akses diizinkan');
class C_master_upload extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Loginauth');
    $this->loginauth->view_page();
    
    $this->load->model("kasubag/Master_upload",'mdl');
  }

  public function index()
  {
    $data['beasiswa'] = $this->mdl->listBeasiswa();
    $data['tahun']    = $this->mdl->get_tahun();
    $this->load->view('attribute/header_kasubag');
    $this->load->view('kasubag/master_upload',$data);
    $this->load->view('attribute/footer');
  }

  public function datatable(){
    $fetch_data = $this->mdl->make_datatables();
    $data = array();
    $nmr = 0;
    foreach($fetch_data as $row)
    {
      $nmr+=1;
      $sub_array = array();
      $sub_array[] = $nmr;
      $sub_array[] = $row->namaBerkas;
      $sub_array[] = '
      <button type="button" name="edit" id="'.$row->id.'" onclick="edit('."'".$row->id."'".')" class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="mdi-editor-mode-edit"></i></button>
      <button type="button" name="remove" id="'.$row->id.'" onclick="remove('."'".$row->id."','".$row->namaBerkas."'".')" class="btn-floating waves-effect waves-light red" title="Hapus"><i class="mdi-action-delete">delete</i></button>
      ';
      $data[] = $sub_array;
    }
    $output = array(
      "draw"            =>  intval($_POST["draw"]),
      "recordsTotal"    =>  $this->mdl->get_all_data(),
      "recordsFiltered" =>  $this->mdl->get_filtered_data(),
      "data"            =>  $data
      );
    echo json_encode($output);
  }

  public function add_data()
  {
    $data = array(
      'namaBerkas' => $this->input->post('namaBerkas'),
      );
    $insert = $this->mdl->save_berkas($data);
    echo json_encode(array("status" => TRUE));
  }

  public function edit_data($id)
  {
    $berkas_upload = $this->mdl->get_by_id($id);
    $namaBerkas = $berkas_upload->namaBerkas;
    $id = $berkas_upload->id;

    $upload = $this->mdl->get_by_id_upload_berkas($id);
    $data = array();
    $data [0] = array(
      'namaBerkas' => $namaBerkas,
      'id' => $id
      );
    echo json_encode($data);
  }

  public function update_data()
  {
    $id = $this->input->post('idNamaBerkas');
    $data = array(
      'namaBerkas' => $this->input->post('namaBerkas'),
      );
    $this->mdl->getupdate($id, $data);
    echo json_encode($data);
  }

  public function delete_data($id)
  {
    $this->mdl->delete_by_id($id);
    echo json_encode(array("status" => TRUE));
  }
}
?>
