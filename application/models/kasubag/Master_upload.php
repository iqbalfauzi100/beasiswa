<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_upload extends CI_Model {

  var $table = "berkas_upload";
  var $select_column = array("berkas_upload.id", "berkas_upload.namaBerkas");
  var $order_column = array("berkas_upload.id", "berkas_upload.namaBerkas",null);

  function make_query()
  {
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    if(isset($_POST["search"]["value"]))
    {
      $this->db->like("berkas_upload.id", $_POST["search"]["value"]);
      $this->db->or_like("berkas_upload.namaBerkas", $_POST["search"]["value"]);
    }
    if(isset($_POST["order"]))
    {
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else
    {
      $this->db->order_by('berkas_upload.id', 'DESC');
    }
  }

  function make_datatables(){
    $this->make_query();
    if($_POST["length"] != -1)
    {
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function get_sub_score($id)
  {
    $sql="SELECT upload_berkas.id, upload_berkas.idBea, upload_berkas.namaBerkas FROM upload_berkas WHERE upload_berkas.idBea='".$id."' ORDER BY upload_berkas.id ASC";
    $query = $this->db->query($sql);
    $data = "";
    $level_dark = 1;
    $nmr = 1;
    foreach ($query->result() as $val) {
      $data .= '<div class="chip light-blue darken-'.$level_dark.' white-text">('.$nmr++.') '.$val->namaBerkas.'</div>';
      if ($level_dark==4) {
        $level_dark = 1;
      }else {
        $level_dark+=1;
      }
    }
    return $data;
  }

  public function save_berkas($data)
  {  
    $this->db->insert($this->table,$data);
    return TRUE;
  }

  public function get_by_id($id)
  {
    $this->db->from('berkas_upload');
    $this->db->where('berkas_upload.id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function get_by_id_upload_berkas($id)
  {
    $this->db->from('berkas_upload');
    $this->db->where('berkas_upload.id',$id);
    $query = $this->db->get();
    return $query->result();
  }

  public function getupdate($id,$data)
  {
    $this->db->query("update berkas_upload set namaBerkas = ? where id = ? ", array($data['namaBerkas'],$id));
    unset($id, $data);
  }


  public function insert_sub_kategori($dataSub)
  {
    $this->db->insert('set_sub_kategori_skor', $dataSub);
    return $this->db->insert_id();
  }

  public function delete_sub_kategori($idSub)
  {
    $this->db->where('id', $idSub);
    $this->db->delete('set_sub_kategori_skor');
  }

  public function delete_by_id($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    #karena db sudah relasi jadi query dibawah di komen
    // $this->db->where('idKategoriSkor', $id);
		// $this->db->delete('set_sub_kategori_skor');
  }

  public function listBeasiswa()
  {
    $data = $this->db->query("SELECT id,namaBeasiswa from bea order by id desc");
    if ($data) {
      return $data->result_array();
    }else{
      return false;
    }
  }

  public function get_tahun()
  {
    $sql = 'SELECT YEAR(bea.beasiswaDibuka) tahun FROM `bea` GROUP BY tahun ORDER BY tahun DESC';
    $res = $this->db->query($sql);
    return $res->result();
  }

  function get_beasiswa($tahun) {
    $getbea ="select bea.id,bea.namaBeasiswa,YEAR(bea.beasiswaDibuka) tahun from upload_berkas,bea where YEAR(bea.beasiswaDibuka)='".$tahun."' and bea.id=upload_berkas.idBea GROUP by namaBeasiswa order by bea.id DESC";
    return $this->db->query($getbea)->result();
  }

}
