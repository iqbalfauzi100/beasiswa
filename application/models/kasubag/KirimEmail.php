<?php
defined('BASEPATH')OR exit('no direct script access allowed');

class KirimEmail extends CI_Model {

    var $table = "pendaftar";
    var $select_column = array("pendaftar.nim","pendaftar.id","pendaftar.idBea", "identitas_mhs.namaLengkap","identitas_mhs.emailAktif","identitas_mhs.jenisKel","YEAR(bea.beasiswaDibuka) tahun", "jurusan.namaJur", "fakultas.namaFk", "bea.penyelenggaraBea","bea.namaBeasiswa");
    var $order_column = array("pendaftar.nim","pendaftar.id","pendaftar.idBea", "identitas_mhs.namaLengkap","identitas_mhs.emailAktif","identitas_mhs.jenisKel","YEAR(bea.beasiswaDibuka) tahun", "jurusan.namaJur", "fakultas.namaFk", "bea.penyelenggaraBea","bea.namaBeasiswa",null);
    var $column_search = array("pendaftar.nim","pendaftar.id","pendaftar.idBea", "identitas_mhs.namaLengkap","identitas_mhs.emailAktif","identitas_mhs.jenisKel","YEAR(bea.beasiswaDibuka) tahun", "jurusan.namaJur", "fakultas.namaFk", "bea.penyelenggaraBea","bea.namaBeasiswa");

    function __construct() {
        parent::__construct();
    }

    public function make_query1($tahun, $fakultas, $jurusan, $bea,$jenisKel) {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        $this->db->join('identitas_mhs', 'identitas_mhs.nimMhs = pendaftar.nim', 'left');
        $this->db->join('jurusan', 'identitas_mhs.idJrs = jurusan.id', 'left');
        $this->db->join('bea', 'bea.id = pendaftar.idBea', 'left');
        $this->db->join('fakultas', 'fakultas.id = jurusan.idFk', 'left');
        $this->db->where("pendaftar.status=1");

        //select 1
        if ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
            $this->db->where("fakultas.id", $fakultas);
        } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
            $this->db->where("jurusan.id", $jurusan);
        } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
            $this->db->where("bea.id", $bea);
        } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);

        //select 2
        } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("fakultas.id", $fakultas);
        } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("jurusan.id", $jurusan);
        } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("bea.id", $bea);
        } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);
        } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("jurusan.id", $jurusan);
        } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("bea.id", $bea);
        } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);
        } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
            $this->db->where("jurusan.id", $jurusan);
            $this->db->where("bea.id", $bea);
        } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
            $this->db->where("jurusan.id", $jurusan);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);
        } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
            $this->db->where("bea.id", $bea);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);

        //select 3
        } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("jurusan.id", $jurusan);
        } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("bea.id", $bea);
        } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);
        } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("jurusan.id", $jurusan);
            $this->db->where("bea.id", $bea);
        } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
            $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
            $this->db->where("jurusan.id", $jurusan);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);
        } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("jurusan.id", $jurusan);
            $this->db->where("bea.id", $bea);
        } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0  && $jenisKel != 0) {
            $this->db->where("fakultas.id", $fakultas);
            $this->db->where("jurusan.id", $jurusan);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);
        } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0  && $jenisKel != 0) {
            $this->db->where("jurusan.id", $jurusan);
            $this->db->where("bea.id", $bea);
            $this->db->where("identitas_mhs.jenisKel", $jenisKel);
        } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
           $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
           $this->db->where("bea.id", $bea);
           $this->db->where("identitas_mhs.jenisKel", $jenisKel);
       } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
           $this->db->where("fakultas.id", $fakultas);
           $this->db->where("bea.id", $bea);
           $this->db->where("identitas_mhs.jenisKel", $jenisKel);

        //select 4
       } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
    } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("bea.id", $bea);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);

        //select 5
    } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    }
    $query = $this->db->get();
    return $query->result();
}

public function make_query($tahun, $fakultas, $jurusan, $bea,$jenisKel) {
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('identitas_mhs', 'identitas_mhs.nimMhs = pendaftar.nim', 'left');
    $this->db->join('jurusan', 'identitas_mhs.idJrs = jurusan.id', 'left');
    $this->db->join('bea', 'bea.id = pendaftar.idBea', 'left');
    $this->db->join('fakultas', 'fakultas.id = jurusan.idFk', 'left');
    $this->db->where("pendaftar.status=1");

    //select 1
    if ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
    } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
        $this->db->where("fakultas.id", $fakultas);
    } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
        $this->db->where("jurusan.id", $jurusan);
    } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("bea.id", $bea);
    } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);

    //select 2
    } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
    } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("jurusan.id", $jurusan);
    } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("bea.id", $bea);
    } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
    } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("bea.id", $bea);
    } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
    } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
        $this->db->where("bea.id", $bea);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);

    //select 3
    } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
    } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("bea.id", $bea);
    } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
    } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
        $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
    } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0  && $jenisKel != 0) {
        $this->db->where("fakultas.id", $fakultas);
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0  && $jenisKel != 0) {
        $this->db->where("jurusan.id", $jurusan);
        $this->db->where("bea.id", $bea);
        $this->db->where("identitas_mhs.jenisKel", $jenisKel);
    } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
       $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
       $this->db->where("bea.id", $bea);
       $this->db->where("identitas_mhs.jenisKel", $jenisKel);
   } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
       $this->db->where("fakultas.id", $fakultas);
       $this->db->where("bea.id", $bea);
       $this->db->where("identitas_mhs.jenisKel", $jenisKel);

    //select 4
   } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
    $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
    $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("bea.id", $bea);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
    $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);

    //select 5
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
    $this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
}

$i = 0;
foreach ($this->column_search as $item) { 
    if ($_POST['search']['value']) {
        if ($i === 0) { 
            $this->db->group_start();
            $this->db->like($item, $_POST['search']['value']);
        } else {
            $this->db->or_like($item, $_POST['search']['value']);
        }
        if (count($this->column_search) - 1 == $i)
            $this->db->group_end();
    }
    $i++;
}

if (isset($_POST["order"])) {
    $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
} else {
    $this->db->order_by('pendaftar.nim', 'DESC');
}
}

public function make_datatables($tahun, $fakultas, $jurusan, $beasiswa,$jenisKel) {
    $this->make_query($tahun, $fakultas, $jurusan, $beasiswa,$jenisKel);
    if ($_POST["length"] != -1) {
        $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
}

public function get_filtered_data($tahun, $fakultas, $jurusan, $beasiswa,$jenisKel) {
    $this->make_query($tahun, $fakultas, $jurusan, $beasiswa,$jenisKel);
    $query = $this->db->get();
    return $query->num_rows();
}

public function get_all_data() {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
}
}
?>
