<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seleksi extends CI_Model {

  var $table = "skor_mahasiswa";
  var $select_column = array("skor_mahasiswa.nimMhs", "skor_mahasiswa.namaLengkap", "skor_mahasiswa.namaBeasiswa", "skor_mahasiswa.ipk", "skor_mahasiswa.skor", "skor_mahasiswa.jumlah as jumlah", "skor_mahasiswa.idBeasiswa", "skor_mahasiswa.updated", "skor_mahasiswa.status", "skor_mahasiswa.idPendaftar","fakultas.namaFk");
  var $order_column = array(null, "skor_mahasiswa.nimMhs", "skor_mahasiswa.namaLengkap", "skor_mahasiswa.ipk", "skor_mahasiswa.skor", "skor_mahasiswa.jumlah", "skor_mahasiswa.updated", null);
  var $column_search = array("skor_mahasiswa.nimMhs", "skor_mahasiswa.namaLengkap", "skor_mahasiswa.ipk", "skor_mahasiswa.skor", "skor_mahasiswa.jumlah");

  function make_query($idBea,$idFak,$idJur,$idAngkatan)
  {
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('identitas_mhs', 'identitas_mhs.nimMhs = skor_mahasiswa.nimMhs', 'left');
    $this->db->join('jurusan', 'identitas_mhs.idJrs = jurusan.id', 'left');
    $this->db->join('fakultas', 'fakultas.id = jurusan.idFk', 'left');
    
    //select 0
    if ($idBea == 0 && $idFak == 0 && $idJur == 0 && $idAngkatan == 0){

    //select 1
    } elseif ($idBea != 0 && $idFak == 0 && $idJur == 0 && $idAngkatan == 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);

    //select 2
    } elseif ($idBea != 0 && $idFak != 0 && $idJur == 0 && $idAngkatan == 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);
      $this->db->where("fakultas.id",$idFak);
    } elseif ($idBea != 0 && $idFak == 0 && $idJur != 0 && $idAngkatan == 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);
      $this->db->where("jurusan.id",$idJur);
    } elseif ($idBea != 0 && $idFak == 0 && $idJur == 0 && $idAngkatan != 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);
      $this->db->where("identitas_mhs.angkatan",$idAngkatan);

    //select 3
    } elseif ($idBea != 0 && $idFak != 0 && $idJur != 0 && $idAngkatan == 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);
      $this->db->where("fakultas.id",$idFak);
      $this->db->where("jurusan.id",$idJur);
    } elseif ($idBea != 0 && $idFak != 0 && $idJur == 0 && $idAngkatan != 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);
      $this->db->where("fakultas.id",$idFak);
      $this->db->where("identitas_mhs.angkatan",$idAngkatan);
    } elseif ($idBea != 0 && $idFak == 0 && $idJur != 0 && $idAngkatan != 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);
      $this->db->where("jurusan.id",$idJur);
      $this->db->where("identitas_mhs.angkatan",$idAngkatan);
    } elseif ($idBea == 0 && $idFak != 0 && $idJur != 0 && $idAngkatan != 0) {
      $this->db->where("fakultas.id",$idFak);
      $this->db->where("jurusan.id",$idJur);
      $this->db->where("identitas_mhs.angkatan",$idAngkatan);
    
    //select 3
    } elseif ($idBea != 0 && $idFak != 0 && $idJur != 0 && $idAngkatan != 0) {
      $this->db->where("skor_mahasiswa.idBeasiswa",$idBea);
      $this->db->where("fakultas.id",$idFak);
      $this->db->where("jurusan.id",$idJur);
      $this->db->where("identitas_mhs.angkatan",$idAngkatan);
    }  
    $i = 0;
    foreach ($this->column_search as $item) // loop column
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
        $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST["order"]))
    {
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else
    {
      $this->db->order_by('jumlah', 'DESC');
    }
  }

  function make_datatables($idBea,$idFak,$idJur,$idAngkatan){
    $this->make_query($idBea,$idFak,$idJur,$idAngkatan);
    if($_POST["length"] != -1)
    {
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function get_filtered_data($idBea,$idFak,$idJur,$idAngkatan){
    $this->make_query($idBea,$idFak,$idJur,$idAngkatan);
    $query = $this->db->get();
    return $query->num_rows();
  }

  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  //combobox
  public function getComboBea()
  {
    $sql = 'SELECT * FROM bea WHERE (selektor="1" || selektor="3") && (CURRENT_TIMESTAMP>bea.beasiswaTutup && CURRENT_DATE<=bea.seleksiTutup) && bea.statusBeasiswa="3" ORDER BY id DESC';
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function getComboFak()
  {
    $sql = 'SELECT * FROM fakultas';
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function getComboAngkatan()
  {
    $sql = 'SELECT angkatan FROM identitas_mhs GROUP BY angkatan ORDER BY angkatan DESC';
    $query = $this->db->query($sql);
    return $query->result();
  }
  public function seleksi_penerima($where, $data)
  {
    $this->db->update("pendaftar", $data, $where);
    return $this->db->affected_rows();
  }

  public function infoDiterima($idBea)
  {
    $sql = "SELECT COUNT(status) diterima FROM `pendaftar` WHERE idBea=".$idBea." && status=1";
    $query = $this->db->query($sql);
    return $query->row()->diterima;
  }

  public function view_detail_score($idPendaftar,$idBea)
  {
    $sql = 'SELECT pendaftar_skor.idBea, pendaftar.nim, pendaftar.id, kategori_skor.nama kategori, set_sub_kategori_skor.nama pilihan, set_sub_kategori_skor.skor FROM `pendaftar_skor`
    LEFT JOIN pendaftar ON pendaftar.id=pendaftar_skor.idPendaftar
    LEFT JOIN kategori_skor ON pendaftar_skor.idKategori=kategori_skor.id
    LEFT JOIN set_sub_kategori_skor ON pendaftar_skor.idSubKategori=set_sub_kategori_skor.id
    WHERE pendaftar_skor.idPendaftar = '.$idPendaftar.' && pendaftar_skor.idBea = '.$idBea;
    $res = $this->db->query($sql);
    return $res->result();
  }
  public function view_detail_upload($idPendaftar,$idBea)
  {
    $sql = 'SELECT pendaftar_upload.idBea, pendaftar.nim, pendaftar.id, pendaftar_upload.idBerkas, berkas_upload.namaBerkas, pendaftar_upload.title FROM pendaftar_upload
    LEFT JOIN pendaftar ON pendaftar.id=pendaftar_upload.idPendaftar
    LEFT JOIN berkas_upload ON berkas_upload.id=pendaftar_upload.idBerkas
    WHERE pendaftar_upload.idPendaftar = '.$idPendaftar.' && pendaftar_upload.idBea = '.$idBea;
    $res = $this->db->query($sql);
    return $res->result();
  }

  public function check_status_penerima($nim)
  {
    $sql = 'SELECT pendaftar.nim, bea.namaBeasiswa, pendaftar.status, bea.periodeBerakhir FROM `pendaftar`
    LEFT JOIN bea ON bea.id=pendaftar.idBea
    WHERE CURRENT_DATE<=bea.periodeBerakhir && pendaftar.status=1 && pendaftar.nim="'.$nim.'"';
    $res = $this->db->query($sql);
    return $res->row();
  }

}
