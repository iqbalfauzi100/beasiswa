<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_bukti_transfer extends CI_Model {

  var $table = "bukti_transfer_upload";
  var $select_column = array("bukti_transfer_upload.id","bukti_transfer_upload.nim","identitas_mhs.namaLengkap", "fakultas.namaFk", "jurusan.namaJur", "bea.penyelenggaraBea","bukti_transfer_upload.tahun", "bukti_transfer_upload.idBea","bukti_transfer_upload.tanggal","bukti_transfer_upload.file","bea.namaBeasiswa","identitas_mhs.jenisKel");
  var $order_column = array("bukti_transfer_upload.id","bukti_transfer_upload.nim","identitas_mhs.namaLengkap", "fakultas.namaFk", "jurusan.namaJur", "bea.penyelenggaraBea","bukti_transfer_upload.tahun", "bukti_transfer_upload.idBea","bukti_transfer_upload.tanggal","bukti_transfer_upload.file","bea.namaBeasiswa","identitas_mhs.jenisKel",null);
  var $column_search = array("bukti_transfer_upload.id","bukti_transfer_upload.nim","identitas_mhs.namaLengkap", "fakultas.namaFk", "jurusan.namaJur", "bea.penyelenggaraBea","bukti_transfer_upload.tahun", "bukti_transfer_upload.idBea","bukti_transfer_upload.tanggal","bukti_transfer_upload.file","bea.namaBeasiswa","identitas_mhs.jenisKel");
  var $default_order = "bukti_transfer_upload.id";

  function make_query($tahun, $fakultas, $jurusan, $bea, $jenisKel)
  {
   $this->db->select($this->select_column);
   $this->db->from($this->table);
   $this->db->join('bea', 'bea.id = bukti_transfer_upload.idBea', 'left');
   $this->db->join('identitas_mhs', 'identitas_mhs.nimMhs = bukti_transfer_upload.nim', 'left');
   $this->db->join('jurusan', 'identitas_mhs.idJrs = jurusan.id', 'left');
   $this->db->join('fakultas', 'fakultas.id = jurusan.idFk', 'left');

//select 1
   if ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
  } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
    $this->db->where("fakultas.id", $fakultas);
  } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
    $this->db->where("jurusan.id", $jurusan);
  } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("bea.id", $bea);
  } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 2
  } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("fakultas.id", $fakultas);
  } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("jurusan.id", $jurusan);
  } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("bea.id", $bea);
  } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
  } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
  } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("bea.id", $bea);
  } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
  } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
  } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
  } elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
    $this->db->where("bea.id", $bea);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 3
  } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
  } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("bea.id", $bea);
  } elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
  } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
  } elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
    $this->db->where("bukti_transfer_upload.tahun", $tahun);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
  } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
  } elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0  && $jenisKel != 0) {
    $this->db->where("fakultas.id", $fakultas);
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
  } elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0  && $jenisKel != 0) {
    $this->db->where("jurusan.id", $jurusan);
    $this->db->where("bea.id", $bea);
    $this->db->where("identitas_mhs.jenisKel", $jenisKel);
  } elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
   $this->db->where("bukti_transfer_upload.tahun", $tahun);
   $this->db->where("bea.id", $bea);
   $this->db->where("identitas_mhs.jenisKel", $jenisKel);
 } elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
   $this->db->where("fakultas.id", $fakultas);
   $this->db->where("bea.id", $bea);
   $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 4
 } elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 5
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
}

$i = 0;
foreach ($this->column_search as $item){
  if($_POST['search']['value']){
    if($i===0){
      $this->db->group_start();
      $this->db->like($item, $_POST['search']['value']);
    } else{
      $this->db->or_like($item, $_POST['search']['value']);
    }
    if(count($this->column_search) - 1 == $i)
      $this->db->group_end();
  }
  $i++;
}

if(isset($_POST["order"])){
  $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
} else{
  $this->db->order_by($this->default_order, 'DESC');
}
}

function make_datatables($tahun, $fakultas, $jurusan, $bea, $jenisKel){
 $this->make_query($tahun, $fakultas, $jurusan, $bea, $jenisKel);
 if($_POST['length'] != -1){
  $this->db->limit($_POST['length'], $_POST['start']);
}
$query = $this->db->get();
return $query->result();
}

function get_filtered_data($tahun, $fakultas, $jurusan, $bea, $jenisKel){
  $this->make_query($tahun, $fakultas, $jurusan, $bea, $jenisKel);
  $query = $this->db->get();
  return $query->num_rows();
}

function get_all_data()
{
 $this->db->select("*");
 $this->db->from($this->table);
 return $this->db->count_all_results();
}

public function get_sub_score($id)
{
  $sql="SELECT upload_berkas.id, upload_berkas.idBea, upload_berkas.namaBerkas FROM upload_berkas WHERE upload_berkas.idBea='".$id."' ORDER BY upload_berkas.id ASC";
  $query = $this->db->query($sql);
  $data = "";
  $level_dark = 1;
  $nmr = 1;
  foreach ($query->result() as $val) {
    $data .= '<div class="chip light-blue darken-'.$level_dark.' white-text">('.$nmr++.') '.$val->namaBerkas.'</div>';
    if ($level_dark==4) {
      $level_dark = 1;
    }else {
      $level_dark+=1;
    }
  }
  return $data;
}

public function save_berkas($data)
{  
  $this->db->insert($this->table,$data);
  return $this->db->insert_id();
}

public function get_by_id($id)
{
  $this->db->from($this->table);
  $this->db->where('bukti_transfer_upload.id',$id);
  $query = $this->db->get();
  return $query->row();
}

public function get_by_id_upload_berkas($id)
{
  $this->db->from($this->table);
  $this->db->where('bukti_transfer_upload.id',$id);
  $query = $this->db->get();
  return $query->result();
}

function get_update($data,$where){
  $this->db->where($where);
  $this->db->update($this->table, $data);
  return TRUE;
}
public function getupdate($key,$data)
{
  $this->db->where('id',$key);
  $this->db->update($this->table,$data);
}

public function insert_sub_kategori($dataSub)
{
  $this->db->insert('set_sub_kategori_skor', $dataSub);
  return $this->db->insert_id();
}

public function delete_sub_kategori($idSub)
{
  $this->db->where('id', $idSub);
  $this->db->delete('set_sub_kategori_skor');
}

public function delete_by_id($id)
{
  $this->db->where('id', $id);
  $this->db->delete($this->table);
}

public function listBeasiswa()
{
  $data = $this->db->query("SELECT id,namaBeasiswa from bea order by id desc");
  if ($data) {
    return $data->result_array();
  }else{
    return false;
  }
}

public function get_tahun()
{
  $sql = 'SELECT YEAR(bea.beasiswaDibuka) tahun FROM `bea` GROUP BY tahun ORDER BY tahun DESC';
  $res = $this->db->query($sql);
  return $res->result();
}

function master_print($tahun, $fakultas, $jurusan, $bea, $jenisKel)
{
 $this->db->select($this->select_column);
 $this->db->from($this->table);
 $this->db->join('bea', 'bea.id = bukti_transfer_upload.idBea', 'left');
 $this->db->join('identitas_mhs', 'identitas_mhs.nimMhs = bukti_transfer_upload.nim', 'left');
 $this->db->join('jurusan', 'identitas_mhs.idJrs = jurusan.id', 'left');
 $this->db->join('fakultas', 'fakultas.id = jurusan.idFk', 'left');

//select 1
 if ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
  $this->db->where("fakultas.id", $fakultas);
} elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
  $this->db->where("jurusan.id", $jurusan);
} elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("bea.id", $bea);
} elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 2
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("jurusan.id", $jurusan);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("bea.id", $bea);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("bea.id", $bea);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
} elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 3
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("bea.id", $bea);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0  && $jenisKel != 0) {
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0  && $jenisKel != 0) {
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
 $this->db->where("bukti_transfer_upload.tahun", $tahun);
 $this->db->where("bea.id", $bea);
 $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0  && $jenisKel != 0) {
 $this->db->where("fakultas.id", $fakultas);
 $this->db->where("bea.id", $bea);
 $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 4
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel == 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea == 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan == 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun != 0 && $fakultas == 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
} elseif ($tahun == 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);

//select 5
} elseif ($tahun != 0 && $fakultas != 0 && $jurusan != 0 && $bea != 0 && $jenisKel != 0) {
  $this->db->where("bukti_transfer_upload.tahun", $tahun);
  $this->db->where("fakultas.id", $fakultas);
  $this->db->where("jurusan.id", $jurusan);
  $this->db->where("bea.id", $bea);
  $this->db->where("identitas_mhs.jenisKel", $jenisKel);
}
$query = $this->db->get();
return $query->result();
}

}
