<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatusBea extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	var $table = "bea";
	var $select_column = array("bea.id", "bea.namaBeasiswa", "bea.penyelenggaraBea", "bea.selektor", "bea.keterangan", "bea.statusBeasiswa","bea.beasiswaDibuka","bea.beasiswaTutup","bea.periodeBerakhir","bea.progress","YEAR(bea.beasiswaDibuka) tahun");
	var $order_column = array("bea.id", "bea.namaBeasiswa", "bea.penyelenggaraBea", "bea.selektor", "bea.keterangan", "bea.statusBeasiswa","bea.beasiswaDibuka","bea.beasiswaTutup","bea.periodeBerakhir",null);
	var $column_search = array("bea.id", "bea.namaBeasiswa", "bea.penyelenggaraBea", "bea.selektor", "bea.keterangan", "bea.statusBeasiswa","bea.beasiswaDibuka","bea.beasiswaTutup","bea.periodeBerakhir");


	function make_query($tahun, $bea)
	{
		$this->db->select($this->select_column);
		$this->db->from($this->table);

		if ($tahun == 0 && $bea == 0) {

		} elseif ($tahun != 0 && $bea == 0) {
			$this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
		} elseif ($tahun == 0 && $bea != 0) {
			$this->db->where("bea.id", $bea);
		}elseif ($tahun != 0 && $bea != 0) {
			$this->db->where("YEAR(bea.beasiswaDibuka)", $tahun);
			$this->db->where("bea.id", $bea);
		}

		$i = 0;
	    foreach ($this->column_search as $item) // loop column
	    {
	      if($_POST['search']['value']) // if datatable send POST for search
	      {

	        if($i===0) // first loop
	        {
	          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
	          $this->db->like($item, $_POST['search']['value']);
	      }
	      else
	      {
	      	$this->db->or_like($item, $_POST['search']['value']);
	      }

	        if(count($this->column_search) - 1 == $i) //last loop
	        $this->db->group_end(); //close bracket
	    }
	    $i++;
	}

	if(isset($_POST["order"]))
	{
		$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	}
	else
	{
		$this->db->order_by('bea.id', 'DESC');
	}
}

function make_datatables($tahun, $bea){
	$this->make_query($tahun, $bea);
	if($_POST["length"] != -1)
	{
		$this->db->limit($_POST['length'], $_POST['start']);
	}
	$query = $this->db->get();
	return $query->result();
}

function get_filtered_data($tahun, $bea){
	$this->make_query($tahun, $bea);
	$query = $this->db->get();
	return $query->num_rows();
}

function get_all_data()
{
	$this->db->select("*");
	$this->db->from($this->table);
	return $this->db->count_all_results();
}

public function get_by_id_bea($id)
{
	$this->db->from($this->table);
	$this->db->where('bea.id',$id);
	$query = $this->db->get();
	return $query->row();
}

public function get_scoring()
{
	$this->db->select('*');
	$this->db->from('kategori_skor');
	$query = $this->db->get();
	return $query->result();
}
public function get_jurusan($fakultas)
{
	$getjur ="select j.id,j.namaJur,f.namaFk from jurusan j,fakultas f where j.idFk=f.id and f.id='$fakultas' order by j.namaJur asc";
	return $this->db->query($getjur)->result();
}



}
