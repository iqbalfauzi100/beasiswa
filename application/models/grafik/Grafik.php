<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Model
{

  public function get_tahun()
  {
    // SELECT YEAR(bea.beasiswaDibuka) tahun FROM `bea` GROUP BY tahun
    $sql = 'SELECT YEAR(bea.beasiswaDibuka) tahun FROM `bea` GROUP BY tahun ORDER BY tahun DESC';
    $res = $this->db->query($sql);
    return $res->result();
  }

  public function get_maxfile()
  {
    // SELECT YEAR(bea.beasiswaDibuka) tahun FROM `bea` GROUP BY tahun
    $sql = 'SELECT file FROM `jadwal_upload` WHERE id=(SELECT MAX(id) FROM jadwal_upload)';
    $res = $this->db->query($sql);
    return $res->row();
  }

  public function get_maxtahun()
  {
    // SELECT YEAR(bea.beasiswaDibuka) tahun FROM `bea` GROUP BY tahun
    $sql = 'SELECT YEAR(jadwal_upload.tanggal) tahun FROM `jadwal_upload` WHERE id=(SELECT MAX(id) FROM jadwal_upload)';
    $res = $this->db->query($sql);
    return $res->row();
  }

  public function cek_ondb()
  {
    $this->db->select("*");
    $this->db->from('jadwal_upload');
    $this->db->where("jadwal_upload.id=(SELECT MAX(id) FROM jadwal_upload)");
    return $this->db->count_all_results();
  }


  // SELECT bea.id, bea.namaBeasiswa,
  // (SELECT COUNT(pendaftar.nim) FROM `pendaftar` WHERE pendaftar.idBea=bea.id && pendaftar.status="1") penerima,
  // (SELECT COUNT(pendaftar.nim) FROM `pendaftar` WHERE pendaftar.idBea=bea.id) mhsDaftar
  // FROM `bea`
  // WHERE YEAR(bea.beasiswaDibuka)="2017"
  public function get_data_grafik($thn)
  {
    $sql = '
    SELECT bea.id, bea.namaBeasiswa, bea.penyelenggaraBea,
    (SELECT COUNT(pendaftar.nim) FROM `pendaftar` WHERE pendaftar.idBea=bea.id && pendaftar.status="1") penerima,
    (SELECT COUNT(pendaftar.nim) FROM `pendaftar` WHERE pendaftar.idBea=bea.id) mhsDaftar
    FROM `bea`
    WHERE YEAR(bea.beasiswaDibuka)="'.$thn.'"
    ';
    $res = $this->db->query($sql);
    return $res->result();
  }

}
