<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_upload extends CI_Model {

  var $table = "jadwal_upload";
  var $select_column = array("jadwal_upload.id", "jadwal_upload.judul","jadwal_upload.tanggal","jadwal_upload.file");
  var $order_column = array("jadwal_upload.id", "jadwal_upload.judul","jadwal_upload.tanggal","jadwal_upload.file",null);
  var $column_search = array("jadwal_upload.id", "jadwal_upload.judul","jadwal_upload.tanggal","jadwal_upload.file");
  var $default_order = "jadwal_upload.id";

  function make_query()
  {
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    // $this->db->where("jadwal_upload.id=(SELECT MAX(id) FROM jadwal_upload)");
    
    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
        $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }

  function make_datatables(){
    $this->make_query();
    if($_POST['length'] != -1){
        $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function get_sub_score($id)
  {
    $sql="SELECT upload_berkas.id, upload_berkas.idBea, upload_berkas.namaBerkas FROM upload_berkas WHERE upload_berkas.idBea='".$id."' ORDER BY upload_berkas.id ASC";
    $query = $this->db->query($sql);
    $data = "";
    $level_dark = 1;
    $nmr = 1;
    foreach ($query->result() as $val) {
      $data .= '<div class="chip light-blue darken-'.$level_dark.' white-text">('.$nmr++.') '.$val->namaBerkas.'</div>';
      if ($level_dark==4) {
        $level_dark = 1;
      }else {
        $level_dark+=1;
      }
    }
    return $data;
  }

  public function save_berkas($data)
  {  
    $this->db->insert($this->table,$data);
    return $this->db->insert_id();
  }

  public function get_by_id($id)
  {
    $this->db->from('jadwal_upload');
    $this->db->where('jadwal_upload.id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function get_by_id_upload_berkas($id)
  {
    $this->db->from('jadwal_upload');
    $this->db->where('jadwal_upload.id',$id);
    $query = $this->db->get();
    return $query->result();
  }

  function get_update($data,$where){
    $this->db->where($where);
    $this->db->update('jadwal_upload', $data);
    return TRUE;
  }
  public function getupdate($key,$data)
  {
    $this->db->where('id',$key);
    $this->db->update('jadwal_upload',$data);
  }

  public function insert_sub_kategori($dataSub)
  {
    $this->db->insert('set_sub_kategori_skor', $dataSub);
    return $this->db->insert_id();
  }

  public function delete_sub_kategori($idSub)
  {
    $this->db->where('id', $idSub);
    $this->db->delete('set_sub_kategori_skor');
  }

  public function delete_by_id($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
  }

  public function listBeasiswa()
  {
    $data = $this->db->query("SELECT id,namaBeasiswa from bea order by id desc");
    if ($data) {
      return $data->result_array();
    }else{
      return false;
    }
  }

  public function get_tahun()
  {
    $sql = 'SELECT YEAR(bea.beasiswaDibuka) tahun FROM `bea` GROUP BY tahun ORDER BY tahun DESC';
    $res = $this->db->query($sql);
    return $res->result();
  }

}
