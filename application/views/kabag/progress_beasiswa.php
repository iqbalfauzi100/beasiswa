<main xmlns="http://www.w3.org/1999/html">
  <div class="container">
    <h3><span class="blue-text">Progress Beasiswa</span></h3>
    <!--  Tables Section-->
    <div id="dashboard">
      <div class="section">

        <form action="<?php echo base_url('mahasiswa/C_mahasiswa/searchFilter'); ?>" method="post" class="col s12">
          <div class="col m12 s12">
            <div class="row">
              <div class="card-panel">
                <div class="row">                        
                  <div class="col s6">
                    <label>Tahun</label>
                    <select class="browser-default"  id="tahun" name="tahun" onChange="viewTabel()">
                      <option value="" disabled selected>Pilih Tahun</option>
                      <?php
                      foreach ($tahun as $row) {
                        ;?>
                        <option value="<?php echo $row->tahun;?>"><?php echo $row->tahun;?></option>
                        <?php }?>
                      </select>
                      
                    </div>
                    <div class="col s6">
                      <label>Beasiswa</label>
                      <select class="browser-default" id="beasiswa" name="beasiswa" onChange="viewTabel()">
                        <option value="">Pilihlah Beasiswa</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>

          <div class="row">
            <div class="col s12">
              <table class="striped" id="tabel">
                <thead>
                  <tr>
                    <th data-field="id" style="width: 3%;">#</th>
                    <th data-field="beasiswa">Beasiswa</th>
                    <th data-field="penyelenggara">Penyelenggara</th>
                    <th data-field="beaBuka">Beasiswa Buka</th>
                    <th data-field="beaTutup">Beasiswa Tutup</th>
                    <th data-field="periode">Periode</th>
                    <th data-field="Status">Progress</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- container END -->
</main>
<script type="text/javascript">
  var save_method;
  var arr = 0;
  var dataTable;
  var tahun;
  var fakultas;
  var jurusan;
  var beasiswa;
  var myVar;

  document.addEventListener("DOMContentLoaded", function (event) {
    // datatable();
  });
  function viewTabel() {
    tahun = $("#tahun").val();
    beasiswa = $("#beasiswa").val();

    datatable();

    reloadJs('materialize', 'min');
    reloadJs('initialize', 'nomin');
  }

  function myTimer() {
    reload_table();
  }

  function datatable() {
    dataTable = $('#tabel').DataTable({
      "destroy":true,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        url:"<?php echo base_url('kabag/C_progress/datatable');?>",
        type: "POST",
        data:{'tahun':tahun,'beasiswa':beasiswa}
      },
      "columnDefs": [
      {
        "targets": [2, -1],
        "orderable": false,
      },
      ],
      "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
    });
  }
  
  function reload_table() {
    dataTable.ajax.reload(null, false);
  }

  function progress(id) {
    var idBea=id;
    var name;

    if (idBea==1){
      name = "Pendaftaran";
    }else if(idBea==2){
      name = "Seleksi";
    }else if(idBea==3){
      name = "Kelulusan";
    }else {
      name = "Pencairan";
    }

    swal({
      title: 'Beasiswa dalam Proses '+name,
      width: 600,
      padding: '3em',
      background: '#fff url(/images/trees.png)',
      backdrop: `
      rgba(0,0,123,0.4)
      url("/images/nyan-cat.gif")
      center left
      no-repeat`
    });
  }

  function close(){
    $('#modal1').closeModal();
  }

  function view_detail_progress(idBea) {
    var url = "<?php echo site_url('kabag/C_progress/view_detail_progress')?>";
    $.ajax({
      url : url+"/"+idBea,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {

        var progress = data.progress;
        var colorProgress = data.colorProgress;
        var namaBeasiswa = data.namaBeasiswa;
        detail = '';

        if(progress==1 && colorProgress==1){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light green"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;
        }else if(progress==1 && colorProgress==2){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light yellow"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==1 && colorProgress==3){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==2 && colorProgress==1){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light green"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==2 && colorProgress==2){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light yellow"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==2 && colorProgress==3){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==3 && colorProgress==1){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light green"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==3 && colorProgress==2){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light yellow"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==3 && colorProgress==3){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light white"></a></td>
          </tr>
          `;

        }else if(progress==4 && colorProgress==1){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light green"></a></td>
          </tr>
          `;

        }else if(progress==4 && colorProgress==2){
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light yellow"></a></td>
          </tr>
          `;

        }else{
          detail += `
          <tr>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
            <td style="text-align: center;"><img src="<?php echo site_url('assets/img/arrow.png')?>"/></td>
            <td style="text-align: center;"><a class="btn-floating btn-large waves-effect waves-light red"></a></td>
          </tr>
          `;

        }

        $('#nim_mhs').html(namaBeasiswa);
        $('#detail_skor_mhs').html(detail);
        $('#modal1').openModal();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data!');
      }
    });
}
</script>
<script type="text/javascript">
  $(document).ready(function(){
   $('#fakultas').change(function(){
    var fakultas =  $('#fakultas').val();
    $.ajax({
      url: '<?php echo base_url('kabag/C_progress/getjurusan'); ?>',
      type: 'GET',
      data: "fakultas="+fakultas,
      dataType: 'json',
      success: function(data){
       var fakultas=`<select id="jurusan" name="jurusan">
       <option value="null">Pilihlah Jurusan</option>`;
       for (var i = 0; i < data.length; i++) {
        fakultas+='<option value="'+data[i].id+'">'+data[i].namaJur+'</option>';
      }
      fakultas+=`</select>
      <label>Jurusan</label>`;
      $('#jurusan').html(fakultas);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });
 });
</script>

<script type="text/javascript">
  $(document).ready(function(){
   $('#tahun').change(function(){
    var tahun =  $('#tahun').val();
    $.ajax({
      url: '<?php echo base_url('kabag/C_progress/getbeasiswa'); ?>',
      type: 'GET',
      data: "tahun="+tahun,
      dataType: 'json',
      success: function(data){
       var tahun=`<select id="beasiswa" name="beasiswa">
       <option value="null">Pilihlah Beasiswa</option>`;
       for (var i = 0; i < data.length; i++) {
        tahun+='<option value="'+data[i].id+'">'+data[i].namaBeasiswa+'</option>';
      }
      tahun+=`</select>
      <label>Jurusan</label>`;
      $('#beasiswa').html(tahun);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });
 });
</script>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
    <h6 class="blue-text">Progress Beasiswa : <span id="nim_mhs"></span> </h6>
    <hr><br>
    <div class="row">
      <table>
        <style media="screen">
          .fon{
            font-weight: bold;
          }
        </style>
        <thead style="font-weight: bold;">
          <tr>
            <td style="text-align: center;">Pendaftaran</td>
            <td style="text-align: center;"></td>
            <td style="text-align: center;">Seleksi</td>
            <td style="text-align: center;"></td>
            <td style="text-align: center;">Kelulusan</td>
            <td style="text-align: center;"></td>
            <td style="text-align: center;">Pencairan</td>
          </tr>
        </thead>
        <tbody id="detail_skor_mhs">

        </tbody>
        <tfoot style="font-weight: bold;">
        </tfoot>
      </table>
    </div>
    <div class="row">                        
      <div class="col s12">
        <b><p>Keterangan :</p>
          <ol>
            <li><font color="green">#</font> Hijau = Belum Dilaksanakan <font color="green">#</font></li>
            <li><font color="yellow">#</font> Kuning = Dalam Proses <font color="yellow">#</font></li>
            <li><font color="red">#</font> Merah = Sudah Selesai<font color="red">#</font></li>
          </ol></b>
        </div>
      </div>
    </div>
  </div>
<!-- Modal Structure -->