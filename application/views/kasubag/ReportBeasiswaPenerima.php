<main id="main">
  <div class="container">
    <h3><span class="blue-text">Laporan Penerima Beasiswa</span></h3>
    <div id="dashboard">
      <div class="section">
        <form action="<?php echo base_url('kasubag/ModulLaporan/searchFilter'); ?>" method="post" class="col s12 m12">
          <div class="row">
            <div class="col s2">
              <label>tahun</label>
              <select class="browser-default"  name="tahun" id="tahun" onChange="viewTabel()">
                <option value="" disabled selected>Pilih Tahun</option>
                <?php
                foreach ($tahun as $row) {
                  ;?>
                  <option value="<?php echo $row->tahun;?>"><?php echo $row->tahun;?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col s2">
                <label>Fakultas</label>
                <select class="browser-default"  name="fakultas" id="fakultas" onChange="viewTabel()">
                  <option value="" disabled selected>Pilih Fakultas</option>
                  <?php foreach ($fakultas as $rowFK): ?>
                    <option value="<?php echo $rowFK['id'] ?>"><?php echo $rowFK['namaFk'] ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col s3">
                <label>jurusan</label>
                <select class="browser-default"  name="jurusan" id="jurusan" onChange="viewTabel()">
                  <option value="" disabled selected>Pilih Jurusan</option>
                </select>
              </div>
              <div class="  col s3">
                <label>Beasiswa</label>
                <select class="browser-default"  id="beasiswa" name="beasiswa" onChange="viewTabel()">
                  <option value="" disabled selected>Pilihlah Beasiswa</option>
                </select>
              </div>
              <div class="col s2">
                <label>Jenis Kelamin</label>
                <select class="browser-default"  id="jenisKel" name="jenisKel" onChange="viewTabel()">
                  <option value="" disabled selected>Pilih J.Kelamin</option>
                  <option value="1">Laki-Laki</option>
                  <option value="2">Perempuan</option>
                </select>
              </div>
            </div>
          </form>
          <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large orange darken-1">
              <i class="large mdi-navigation-unfold-less"></i>
            </a>
            <ul>
              <li><a class="btn-floating green tooltipped" data-position="left" data-delay="50" data-tooltip="Print Excell" onclick="print_excell()"><i class="mdi-maps-local-print-shop"></i></a></li>
              <li><a class="btn-floating red tooltipped" data-position="left" data-delay="50" data-tooltip="Print PDF" onclick="print_laporan();"><i class="mdi-maps-local-print-shop"></i></a></li>
            </ul>
          </div>
          <table class="striped table-responsive highlight bordered" id="tabelBeasiswa">
            <thead>
              <tr>
                <td data-field="id">No</td>
                <td data-field="nim">NIM</td>
                <td data-field="nama">Nama</td>
                <td data-field="fakultas">Fakultas</td>
                <td data-field="jurusan">Jurusan</td>
                <td data-field="ipk">IPK</td>
                <td data-field="berkas">Berkas Upload</td>
                <td data-field="beasiswa">Jenis Beasiswa</td>
                <th data-field="penyelenggara">Penyelenggara</th>
                <th data-field="kelamin">JK</th>
                <td data-field="angkatan">Tahun</td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </main>

  <script type="text/javascript">
    var dataTable;
    var tahun;
    var fakultas;
    var jurusan;
    var beasiswa;
    document.addEventListener("DOMContentLoaded", function (event) {
    // datatable();
  });

    function viewTabel() {
     tahun = $("#tahun").val();
     fakultas = $("#fakultas").val();
     jurusan = $("#jurusan").val();
     beasiswa= $("#beasiswa").val();
     jenisKel = $("#jenisKel").val();

     datatable();

     reloadJs('materialize','min');
     reloadJs('initialize','nomin');
   }

   function myTimer() {
     reload_table();
   }

   function datatable() {
     dataTable = $('#tabelBeasiswa').DataTable({
      "destroy": true,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax":{
       url: "<?php echo base_url('kasubag/ModulLaporan/datatable'); ?>",
       type: "POST",
       data:{'tahun':tahun,'fakultas':fakultas,'jurusan':jurusan,'beasiswa':beasiswa,'jenisKel':jenisKel}
     },
     "columnDefs": [
     {
      "targets": [2,-1],
      "orderable":false,
    },
    ],
    "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>'
  });
   }
   function reload_table() {
     dataTable.ajax.reload(null, false);
   }
   function view_detail_upload(idPendaftar,idBea) {
    var url = "<?php echo site_url('kasubag/ModulLaporan/view_detail_upload')?>";
    $.ajax({
      url : url+"/"+idPendaftar+"/"+idBea,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        detail = '';
        no=0;
        for (var i = 0; i < data.length; i++) {
          no+=1;
          detail += `
          <tr>
            <td>`+no+`</td>
            <td>`+data[i].namaBerkas+`</td>
            <td>`+data[i].title+`</td>
            <td><a href="<?php echo site_url('assets/img/upload_berkas/`+data[i].title+`')?>" target="_blank"><img src="<?php echo site_url('assets/img/upload_berkas/kaca.png')?>" width="50" height="50"/></a></td>
          </tr>
          `;
        }
        $('#nim').html(data[0].nim);
        $('#detail_upload').html(detail);
        $('#modal2').openModal();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data!');
      }
    });
  }
 </script>
 <script>
  function print_laporan() {

    var tahun;
    var fakultas;
    var jurusan;
    var beasiswa;
    var jenisKel;

    tahun = $('#tahun').val();
    fakultas = $('#fakultas').val();
    jurusan = $('#jurusan').val();
    beasiswa = $('#beasiswa').val();
    jenisKel = $('#jenisKel').val();

    window.open("<?=site_url()?>kasubag/ModulLaporan/get_data_print/"+tahun+"/"+fakultas+"/"+jurusan+"/"+beasiswa+"/"+jenisKel);

  }
  function print_excell() {

    var tahun;
    var fakultas;
    var jurusan;
    var beasiswa;
    var jenisKel;
    tahun = $('#tahun').val();
    fakultas = $('#fakultas').val();
    jurusan = $('#jurusan').val();
    beasiswa = $('#beasiswa').val();
    jenisKel = $('#jenisKel').val();

    window.open("<?=site_url()?>kasubag/ModulLaporan/get_print_excell_penerima/"+tahun+"/"+fakultas+"/"+jurusan+"/"+beasiswa+"/"+jenisKel);

  }
  $(document).ready(function(){
   $('#fakultas').change(function(){
    var fakultas =  $('#fakultas').val();
    $.ajax({
      url: '<?php echo base_url('kasubag/ModulLaporan/getJurusan'); ?>',
      type: 'GET',
      data: "fakultas="+fakultas,
      dataType: 'json',
      success: function(data){
       var fakultas=`<select id="jurusan" name="jurusan">
       <option value="null">Pilihlah Jurusan</option>`;
       for (var i = 0; i < data.length; i++) {
        fakultas+='<option value="'+data[i].id+'">'+data[i].namaJur+'</option>';
      }
      fakultas+=`</select>
      <label>Jurusan</label>`;
      $('#jurusan').html(fakultas);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });

   $('#tombolPrint').on('click', function(){
     var tahun;
     var fakultas;
     var jurusan;
     var beasiswa;
     tahun = $('#tahun').val();
     fakultas = $('#fakultas').val();
     jurusan = $('#jurusan').val();
     beasiswa = $('#beasiswa').val();
     $.ajax({
      type: 'ajax',
      method: 'GET',
      url : '<?php echo base_url('kasubag/ModulLaporan/DataPenerima'); ?>',
      data: {'thn':tahun,'fk':fakultas,'jrs':jurusan,'bea':beasiswa},
      async: false,
      dataType: 'json',
      success: function(data){

      },
      error: function(e){
        alert('error'+e);
      }
    });

   });
 });
</script>
<script type="text/javascript">
  $(document).ready(function(){
   $('#tahun').change(function(){
    var tahun =  $('#tahun').val();
    $.ajax({
      url: '<?php echo base_url('kasubag/ModulLaporan/getbeasiswa'); ?>',
      type: 'GET',
      data: "tahun="+tahun,
      dataType: 'json',
      success: function(data){
       var tahun=`<select id="beasiswa" name="beasiswa">
       <option value="null">Pilihlah Beasiswa</option>`;
       for (var i = 0; i < data.length; i++) {
        tahun+='<option value="'+data[i].id+'">'+data[i].namaBeasiswa+'</option>';
      }
      tahun+=`</select>
      <label>Beasiswa</label>`;
      $('#beasiswa').html(tahun);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });
 });
</script>
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4>Berkas Upload NIM : <span id="nim"></span> </h4>
    <hr><br>
    <div class="row">
      <div id="gallery">
        <table>
          <style media="screen">
            .fon{
              font-weight: bold;
            }
          </style>
          <thead style="font-weight: bold;">
            <tr>
              <td>No</td>
              <td>Nama Berkas</td>
              <td>File Name</td>
              <td>Lihat Berkas</td>
            </tr>
          </thead>
          <tbody id="detail_upload">

          </tbody>
          <tfoot style="font-weight: bold;">
            <tr>
              <td>No</td>
              <td>Nama Berkas</td>
              <td>File Name</td>
              <td>Lihat Berkas</td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

