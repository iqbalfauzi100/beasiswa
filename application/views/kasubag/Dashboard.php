<!-- Main START -->
<main>
  <div class="container">
    <?php
    $level =$this->session->userdata('level');
    $yos;
    if ($level == 1) {
      $yos="Staff Kemahasiswaan";
    }elseif ($level == 2) {
      $yos="Kasubag Kemahasiswaan";
    }elseif ($level == 3) {
      $yos="Kasubag fakultas";
    }elseif ($level == 4) {
      $yos="Kabag Kemahasiswaan";
    }elseif ($level == 5) {
      $yos="Mahasiswa";
    }else {
      $yos="Admin";
    } ?>
    <!-- <strong><h5 class="thin">Selamat datang, <?php echo $yos; ?></h5></strong> -->
    <div id="dashboard">
      <div class="section">
        <div class="row">
          <div class="col s12">
            <!-- Dropdown Structure -->
            <div class="card-panel secondary-color white-text"><strong>INFO :</strong><br/>
            1. Dimohon untuk mengisi Profil dan mengganti password anda jika belum terisi atau baru pertama login sistem<br/>
            2. Pastikan untuk selalu rutin mengganti password untuk menjamin keamanan akun anda
            </div>
          </div>

          <div class="col s12">
          <center><h5 class="blue-text"><b>Pengumuman Jadwal Penerimaan Beasiswa Tahun <?php echo $tahun;?></b></h5></center><br>
            <center><a href="<?php echo site_url('assets/img/upload_pengumuman/'.$foto)?>" target="_blank"><img src="<?php echo site_url('assets/img/upload_pengumuman/'.$foto)?>" width="600" height="400"/></a></center>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- container END -->

</main>
