<!-- Main START -->
<main>
  <div class="container">
    <div id="dashboard">
      <div class="section">
        <div id="responsive" class="section">
          <div class="row">
            <div class="col s12 m6">
              <h4>
                Bukti Transfer Beasiswa
              </h4>
            </div>
          </div>

          <form action="<?php echo base_url('kasubag/ModulLaporan/searchFilter'); ?>" method="post" class="col s12 m12">
            <div class="row">
              <div class="col s2">
                <label>Tahun</label>
                <select class="browser-default"  name="tahun" id="tahun" onChange="viewTabel()">
                  <option value="" disabled selected>Pilih Tahun</option>
                  <?php
                  foreach ($tahun as $row) {
                    ;?>
                    <option value="<?php echo $row->tahun;?>"><?php echo $row->tahun;?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="col s2">
                  <label>Fakultas</label>
                  <select class="browser-default" name="fakultas" id="fakultas" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih Fakultas</option>
                    <?php foreach ($fakultas as $rowFK): ?>
                      <option value="<?php echo $rowFK['id'] ?>"><?php echo $rowFK['namaFk'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col s3">
                  <label>jurusan</label>
                  <select class="browser-default" name="jurusan" id="jurusan" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih Jurusan</option>
                  </select>
                </div>
                <div class="  col s3">
                  <label>Beasiswa</label>
                  <select class="browser-default"  id="beasiswa" name="beasiswa" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih Beasiswa</option>
                  </select>
                </div>
                <div class="col s2">
                  <label>Jenis Kelamin</label>
                  <select class="browser-default"  id="jenisKel" name="jenisKel" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih J.Kelamin</option>
                    <option value="1">Laki-Laki</option>
                    <option value="2">Perempuan</option>
                  </select>
                </div>
              </div>
            </form>

            <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
              <a class="btn-floating btn-large orange darken-1">
                <i class="large mdi-navigation-unfold-less"></i>
              </a>
              <ul>
                <li><a class="btn-floating green tooltipped" data-position="left" data-delay="50" data-tooltip="Print Excell" onclick="print_excell()"><i class="mdi-maps-local-print-shop"></i></a></li>
                <li><a class="btn-floating red tooltipped" data-position="left" data-delay="50" data-tooltip="Print PDF" onclick="print_laporan();"><i class="mdi-maps-local-print-shop"></i></a></li>
              </ul>
            </div>

            <div class="col s12 m4 l6">
              <?php echo $this->session->flashdata('pesan');?></div>
              <div class="row">
                <div class="col s12">
                  <table class="striped" id="tabel">
                    <thead>
                      <tr>
                        <th data-field="ok" style="width: 3%;">#</th>
                        <th data-field="ok">NIM</th>
                        <td data-field="ok">Nama</td>
                        <td data-field="ok">Fakultas</td>
                        <td data-field="ok">Jurusan</td>
                        <th data-field="ok">Beasiswa</th>
                        <th data-field="ok">Penyelenggara</th>
                        <th data-field="ok">Tanggal Upload</th>
                        <th data-field="ok">File</th>
                        <th data-field="ok">Tahun</th>
                        <th data-field="ok">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- container END -->
    </main>

    <script type="text/javascript">

      var save_method;
      var arr = 0;
      var dataTable;
      var tahun;
      var fakultas;
      var jurusan;
      var beasiswa;
      var jenisKel;

      document.addEventListener("DOMContentLoaded", function(event) {
        // datatable();
      });

      function viewTabel() {
        tahun     = $("#tahun").val();
        fakultas  = $("#fakultas").val();
        jurusan   = $("#jurusan").val();
        beasiswa  = $("#beasiswa").val();
        jenisKel  = $("#jenisKel").val();

        datatable();
        reloadJs('materialize','min');
        reloadJs('initialize','nomin');
      }

      function datatable() {
       dataTable = $('#tabel').DataTable({
        "destroy": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax":{
         url: "<?php echo base_url('kasubag/C_upload_transfer/datatable'); ?>",
         type: "POST",
         data:{'tahun':tahun,'fakultas':fakultas,'jurusan':jurusan,'beasiswa':beasiswa,'jenisKel':jenisKel}
       },
       "columnDefs": [
       {
        "targets": [2,-1],
        "orderable":false,
      },
      ],
      "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>'
    });
     }

     function reload_table(){
      dataTable.ajax.reload(null,false);
    }

    function remove(id, nama) {
      swal({
        title: '"'+nama+'"',
        text: "Apakah anda yakin ingin menghapus bukti transfer beasiswa ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#F44336',
        confirmButtonText: 'Hapus',
        cancelButtonText: "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm){
          $.ajax({
            url : "<?php echo site_url('kasubag/C_upload_transfer/delete_data/')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
              reload_table();
              swal("Terhapus :(", "Data berhasil dihapus!", "success");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              swal("Erorr!", "Terjadi masalah saat penghapusan data!", "error");
            }
          });
        } else {
          swal("Dibatalkan :)", "Penghapusan data dibatalkan!", "error");
        }
      });
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
     $('#tahun').change(function(){
      var tahun =  $('#tahun').val();
      $.ajax({
        url: '<?php echo base_url('kasubag/ModulLaporan/getbeasiswa'); ?>',
        type: 'GET',
        data: "tahun="+tahun,
        dataType: 'json',
        success: function(data){
         var tahun=`<select id="beasiswa" name="beasiswa">
         <option value="null">Pilihlah Beasiswa</option>`;
         for (var i = 0; i < data.length; i++) {
          tahun+='<option value="'+data[i].id+'">'+data[i].namaBeasiswa+'</option>';
        }
        tahun+=`</select>
        <label>Beasiswa</label>`;
        $('#beasiswa').html(tahun);
        reloadJs('materialize','min');
        reloadJs('initialize','nomin');
      }
    });
    });
   });
 </script>

 <script type="text/javascript">
  $(document).ready(function(){
   $('#fakultas').change(function(){
    var fakultas =  $('#fakultas').val();
    $.ajax({
      url: '<?php echo base_url('kasubag/ModulLaporan/getJurusan'); ?>',
      type: 'GET',
      data: "fakultas="+fakultas,
      dataType: 'json',
      success: function(data){
       var fakultas=`<select id="jurusan" name="jurusan">
       <option value="null">Pilihlah Jurusan</option>`;
       for (var i = 0; i < data.length; i++) {
        fakultas+='<option value="'+data[i].id+'">'+data[i].namaJur+'</option>';
      }
      fakultas+=`</select>
      <label>Jurusan</label>`;
      $('#jurusan').html(fakultas);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });
 });
</script>
<script>
  function print_laporan() {

    var tahun;
    var fakultas;
    var jurusan;
    var beasiswa;
    var jenisKel;

    tahun = $('#tahun').val();
    fakultas = $('#fakultas').val();
    jurusan = $('#jurusan').val();
    beasiswa = $('#beasiswa').val();
    jenisKel = $('#jenisKel').val();

    window.open("<?=site_url()?>kasubag/C_upload_transfer/get_data_print/"+tahun+"/"+fakultas+"/"+jurusan+"/"+beasiswa+"/"+jenisKel);

  }
  function print_excell() {

    var tahun;
    var fakultas;
    var jurusan;
    var beasiswa;
    var jenisKel;
    tahun = $('#tahun').val();
    fakultas = $('#fakultas').val();
    jurusan = $('#jurusan').val();
    beasiswa = $('#beasiswa').val();
    jenisKel = $('#jenisKel').val();

    window.open("<?=site_url()?>kasubag/C_upload_transfer/get_print_excell_transfer/"+tahun+"/"+fakultas+"/"+jurusan+"/"+beasiswa+"/"+jenisKel);

  }
</script>