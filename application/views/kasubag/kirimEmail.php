<main>
  <div class="container">
    <h3><span class="blue-text">Kirim Email</span></h3>
    <div id="dashboard">
      <div class="section">
        <div class="col s12 m4 l6">
          <?php echo $this->session->flashdata('pesan');?></div>
          <form action="" method="post" class="col s12 m12">
            <div class="row">
              <div class="col s2">
                <label>Tahun</label>
                <select class="browser-default"  name="tahun" id="tahun" onChange="viewTabel()">
                  <option value="" disabled selected>Pilih Tahun</option>
                  <?php
                  foreach ($tahun as $row) {
                    ;?>
                    <option value="<?php echo $row->tahun;?>"><?php echo $row->tahun;?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="col s2">
                  <label>Fakultas</label>
                  <select class="browser-default"  name="fakultas" id="fakultas" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih Fakultas</option>
                    <?php foreach ($fakultas as $rowFK): ?>
                      <option value="<?php echo $rowFK['id'] ?>"><?php echo $rowFK['namaFk'] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col s3">
                  <label>Jurusan</label>
                  <select class="browser-default" name="jurusan" id="jurusan" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih Jurusan</option>
                  </select>
                </div>
                <div class="  col s3">
                  <label>Beasiswa</label>
                  <select class="browser-default"  id="beasiswa" name="beasiswa" onChange="viewTabel()">
                    <option value="" disabled selected>Pilihlah Beasiswa</option>
                  </select>
                </div>
                <div class="col s2">
                  <label>Jenis Kelamin</label>
                  <select class="browser-default"  id="jenisKel" name="jenisKel" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih J.Kelamin</option>
                    <option value="1">Laki-Laki</option>
                    <option value="2">Perempuan</option>
                  </select>
                </div>
              </div>
            </form>

            <!-- TOMBOL PRINT -->
          <!-- <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large red" onclick="print_laporan();">
             <i class="large material-icons">print</i></a>
           </div> -->
           <!-- END TOMBOL PRINT -->

           <form class="col s12" method="post" action="<?php echo base_url('kasubag/C_email/send_email');?>">
             <div class="col s12 m8 l9">
              <div class="row">
                <div class="row">
                  <div class="input-field col s6">
                    <input id="input_text" type="text" name="subject" required="required">
                    <label for="input_text">Subject Email</label>
                  </div>
                  <div class="input-field col s6">
                    <button type="submit" class="btn green"><i class="mdi-content-send left"></i>Kirim</button>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <textarea id="textarea2" name="message" class="materialize-textarea" required="required"></textarea>
                    <label for="textarea1">Message</label>
                  </div>
                </div>
              </div>
            </div>

            <div class="box-body" style="overflow-x: auto;">
              <table id="tabelBeasiswa" class="table table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                  <tr>
                   <td data-field="id">No</td>
                   <td data-field="nim">NIM</td>
                   <td data-field="nama">Nama</td>
                   <td data-field="fakultas">Fakultas</td>
                   <td data-field="jurusan">Jurusan</td>
                   <td data-field="penyelenggara">Penyelenggara</td>
                   <td data-field="email">Email</td>
                   <td data-field="angkatan">Tahun</td>
                 </tr>
               </thead>
               <tbody>
                <!-- <?php 
                $hp = $this->db->get('identitas_mhs')->result();
                $count = count($hp);
                $no = 1;
                foreach ($hp as $row): ?>
                <tr>
                  <td>
                    <?php echo $no++;?>
                    <input type="hidden" name="email[]" value="<?php echo $row->emailAktif;?>">
                  </td>
                  <td><?php echo $row->nimMhs;?></td>
                  <td><?php echo $row->emailAktif;?></td>
                </tr>
              <?php endforeach; ?> -->
            </tbody>
            <tfoot>
              <tr>
                <td data-field="id">No</td>
                <td data-field="nim">NIM</td>
                <td data-field="nama">Nama</td>
                <td data-field="fakultas">Fakultas</td>
                <td data-field="jurusan">Jurusan</td>
                <td data-field="penyelenggara">Penyelenggara</td>
                <td data-field="email">Email</td>
                <td data-field="angkatan">Tahun</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
</main>
<script type="text/javascript">
  var dataTable;
  var tahun;
  var fakultas;
  var jurusan;
  var beasiswa;
  document.addEventListener("DOMContentLoaded", function (event) {
    // datatable();
  });

  function viewTabel() {
   tahun = $("#tahun").val();
   fakultas = $("#fakultas").val();
   jurusan = $("#jurusan").val();
   beasiswa= $("#beasiswa").val();
   jenisKel = $("#jenisKel").val();

   datatable();

   reloadJs('materialize','min');
   reloadJs('initialize','nomin');
 }

 function myTimer() {
   reload_table();
 }

 function datatable() {
   dataTable = $('#tabelBeasiswa').DataTable({
    "destroy": true,
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax":{
     url: "<?php echo base_url('kasubag/C_email/datatable'); ?>",
     type: "POST",
     data:{'tahun':tahun,'fakultas':fakultas,'jurusan':jurusan,'beasiswa':beasiswa,'jenisKel':jenisKel}
   },
   "columnDefs": [
   {
    "targets": [2,-1],
    "orderable":false,
  },
  ],
  "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>'
});
 }

 function reload_table() {
   dataTable.ajax.reload(null, false);
 }
</script>
<script>
  function print_laporan() {

    var tahun;
    var fakultas;
    var jurusan;
    var beasiswa;
    var jenisKel;

    tahun = $('#tahun').val();
    fakultas = $('#fakultas').val();
    jurusan = $('#jurusan').val();
    beasiswa = $('#beasiswa').val();
    jenisKel = $('#jenisKel').val();

    window.open("<?=site_url()?>kasubag/ModulLaporan/get_data_print/"+tahun+"/"+fakultas+"/"+jurusan+"/"+beasiswa+"/"+jenisKel);

  }

  function print_excell() {

    var tahun;
    var fakultas;
    var jurusan;
    var beasiswa;
    var jenisKel;
    tahun = $('#tahun').val();
    fakultas = $('#fakultas').val();
    jurusan = $('#jurusan').val();
    beasiswa = $('#beasiswa').val();
    jenisKel = $('#jenisKel').val();

    window.open("<?=site_url()?>kasubag/ModulLaporan/get_print_excell_penerima/"+tahun+"/"+fakultas+"/"+jurusan+"/"+beasiswa+"/"+jenisKel);

  }

  $(document).ready(function(){
   $('#fakultas').change(function(){
    var fakultas =  $('#fakultas').val();
    $.ajax({
      url: '<?php echo base_url('kasubag/ModulLaporan/getJurusan'); ?>',
      type: 'GET',
      data: "fakultas="+fakultas,
      dataType: 'json',
      success: function(data){
       var fakultas=`<select id="jurusan" name="jurusan">
       <option value="null">Pilihlah Jurusan</option>`;
       for (var i = 0; i < data.length; i++) {
        fakultas+='<option value="'+data[i].id+'">'+data[i].namaJur+'</option>';
      }
      fakultas+=`</select>
      <label>Jurusan</label>`;
      $('#jurusan').html(fakultas);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });
 });
</script>
<script type="text/javascript">
  $(document).ready(function(){
   $('#tahun').change(function(){
    var tahun =  $('#tahun').val();
    $.ajax({
      url: '<?php echo base_url('kasubag/ModulLaporan/getbeasiswa'); ?>',
      type: 'GET',
      data: "tahun="+tahun,
      dataType: 'json',
      success: function(data){
       var tahun=`<select id="beasiswa" name="beasiswa">
       <option value="null">Pilihlah Beasiswa</option>`;
       for (var i = 0; i < data.length; i++) {
        tahun+='<option value="'+data[i].id+'">'+data[i].namaBeasiswa+'</option>';
      }
      tahun+=`</select>
      <label>Beasiswa</label>`;
      $('#beasiswa').html(tahun);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });
 });
</script>