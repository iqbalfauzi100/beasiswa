<!-- Main START -->
<main>
  <div class="container">
    <h3 style="text-align: center;"><span class="blue-text">Grafik Pendaftar dan Penerima Beasiswa</span></h3>
    <div id="dashboard">
      <div class="section">

        <div class="row">
          <?php
          $nama_bea = array();
          $terima = array();
          $daftar = array();
          foreach ($view_grafik as $vg) {
            $nama_bea[] = $vg->namaBeasiswa.' ('.$vg->penyelenggaraBea.')';
            $terima[] = (int) $vg->penerima;
            $daftar[] = (int) $vg->mhsDaftar;
          }
          ?>
          <div class="col m6">
            <font size="4pt" class="blue-text">Pilih periode beasiswa: </font><br>
            <select class="browser-default" name="pilih_tahun" id="pilih_tahun" onChange="refresh_chart()">
              <option value="kosong">-Pilih Tahun</option>
              <?php
              foreach ($tahun as $th) {
                if ($selected_tahun==$th->tahun) {
                  $selected="selected";
                }else {
                  $selected="";
                }
                echo '<option value="'.$th->tahun.'" '.$selected.'>'.$th->tahun.'</option>';
              }
              ?>
            </select>
          </div>
          <br>
          <br>
          <br>
          <br>
          <div class="col m12" id="graf" style="visibility: visible;">
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>
    </div>

  </main>
  <script type="text/javascript">
    function refresh_chart() {
      id_data = $('#pilih_tahun').val();
      url = "<?php echo site_url('kabag/C_kabag/viewGrafik')?>";
      window.location.href=url+'/'+id_data;
    }
  </script>

  <script type="text/javascript">
    Highcharts.chart('container', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Grafik Pendaftar dan Penerima Beasiswa UIN Malang'
      },
      subtitle: {
        text: 'source: http://kemahasiswaan.uin-malang.ac.id'
      },
      xAxis: {
        categories: <?php echo json_encode($nama_bea); ?>,
      },
      yAxis: {
        title: {
          text: 'Jumlah (Mahasiswa)'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [{
        name: 'Pendaftar',
        data: <?php echo json_encode($daftar)?>
      }, {
        name: 'Penerima',
        data: <?php echo json_encode($terima)?>
      }]
    });
  </script>
