<!-- Main START -->
<main>
  <div class="container">
    <div id="dashboard">
      <div class="section">
        <div id="responsive" class="section">
          <div class="row">
            <div class="col s12 m6">
              <h4>
                Upload Bukti Transfer
                <button class="btn-floating waves-effect waves-light primary-color z-depth-0" onclick="add_data()" title="Tambah Upload Data"><i class="mdi-content-add"></i></button>
              </h4>
            </div>
          </div>
          <div class="card-panel yellow accent-4">Kepada Mahasiswa Penerima dana Beasiswa, diharapkan untuk upload bukti pencairan dana beasiswa rekening mahasiswa</div>
          <div class="col s12 m4 l6">
            <?php echo $this->session->flashdata('pesan');?></div>
            <div class="row">
              <div class="col s12">
                <table class="striped" id="tabel">
                  <thead>
                    <tr>
                      <th data-field="id" style="width: 3%;">#</th>
                      <th data-field="jenis_scoring">Tahun</th>
                      <th data-field="jenis_scoring">Beasiswa</th>
                      <th data-field="jenis_scoring">Tanggal Upload</th>
                      <th data-field="jenis_scoring">File</th>
                      <th data-field="aksi">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Rekening Listrik</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Pekerjaan Ayah</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Pekerjaan Ibu</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- container END -->
  </main>

  <script type="text/javascript">

    var save_method;
    var arr = 0;
    var dataTable;

    document.addEventListener("DOMContentLoaded", function(event) {
      datatable();
    });

    function datatable() {
      dataTable = $('#tabel').DataTable({
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
          url:"<?php echo base_url('mahasiswa/C_upload_transfer/datatable'); ?>",
          type:"POST"
        },
        "columnDefs":[
        {
          "targets":[2,-1],
          "orderable":false,
        },
        ],
        "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
        language : {
          sLengthMenu: "Tampilkan _MENU_",
          sSearch: "Cari:"
        }
      });
    }

    function reload_table(){
      dataTable.ajax.reload(null,false);
    }

    function add_data() {
      arr = 0;
      save_method = 'add';
      $('#formInput')[0].reset();
      $('.form-group').removeClass('has-error');
      $('.blue-text').empty(); 
      $('#modal1').openModal();
    }

    function edit(id) {
      arr = 0;
      save_method = 'update';
      $('#formInput')[0].reset();

      $.ajax({
        url : "<?php echo site_url('mahasiswa/C_upload_transfer/edit_data/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          $('#idUpload').val(data[0].id);
          $('#tanggal2').val(data[0].tanggal);
          $('#pathfile2').val(data[0].file);
          
          reloadJs('materialize', 'min');
          reloadJs('initialize', 'nomin');
          $('#modal2').openModal();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data');
        }
      });
    }

    function remove(id, nama) {
      swal({
        title: '"'+nama+'"',
        text: "Apakah anda yakin ingin menghapus bukti transfer beasiswa ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#F44336',
        confirmButtonText: 'Hapus',
        cancelButtonText: "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm){
          $.ajax({
            url : "<?php echo site_url('mahasiswa/C_upload_transfer/delete_data/')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
              reload_table();
              swal("Terhapus :(", "Data berhasil dihapus!", "success");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              swal("Erorr!", "Terjadi masalah saat penghapusan data!", "error");
            }
          });
        } else {
          swal("Dibatalkan :)", "Penghapusan data dibatalkan!", "error");
        }
      });
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
     $('#tahun').change(function(){
      var tahun =  $('#tahun').val();
      $.ajax({
        url: '<?php echo base_url('mahasiswa/C_upload_transfer/getbeasiswa'); ?>',
        type: 'GET',
        data: "tahun="+tahun,
        dataType: 'json',
        success: function(data){
         var tahun=`<select id="beasiswa" name="beasiswa">
         <option value="null">Pilihlah Beasiswa</option>`;
         for (var i = 0; i < data.length; i++) {
          tahun+='<option value="'+data[i].id+'">'+data[i].namaBeasiswa+'</option>';
        }
        tahun+=`</select>
        <label>Beasiswa</label>`;
        $('#beasiswa').html(tahun);
        reloadJs('materialize','min');
        reloadJs('initialize','nomin');
      }
    });
    });
   });
 </script>

   <script type="text/javascript">
    $(document).ready(function(){
     $('#tahunn').change(function(){
      var tahun =  $('#tahunn').val();
      $.ajax({
        url: '<?php echo base_url('mahasiswa/C_upload_transfer/getbeasiswa'); ?>',
        type: 'GET',
        data: "tahun="+tahun,
        dataType: 'json',
        success: function(data){
         var tahun=`<select id="beasiswaa" name="beasiswaa">
         <option value="null">Pilihlah Beasiswa</option>`;
         for (var i = 0; i < data.length; i++) {
          tahun+='<option value="'+data[i].id+'">'+data[i].namaBeasiswa+'</option>';
        }
        tahun+=`</select>
        <label>Beasiswa</label>`;
        $('#beasiswaa').html(tahun);
        reloadJs('materialize','min');
        reloadJs('initialize','nomin');
      }
    });
    });
   });
 </script>

 <!-- Modal Structure -->
 <div id="modal1" class="modal">
  <div class="modal-content">
    <h4 class="modal-title">Upload Bukti Transfer</h4>
    <hr><br>
    <form method="post" action="<?php echo base_url(); ?>mahasiswa/C_upload_transfer/save_data" id="formInput" enctype=multipart/form-data>
      <div class="row">
       <div class="row">
        <div class="col m1">
          <i class="mdi-av-timer prefix" style="font-size: 2em;"></i>
        </div>
        <div class="col m11">
          <label>Tahun</label>
          <select class="browser-default" name="tahun" id="tahun">
            <option value="" disabled selected>Pilih Tahun</option>
            <?php
            foreach ($tahun as $row) {
              ;?>
              <option value="<?php echo $row->tahun;?>"><?php echo $row->tahun;?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col m1">
            <i class="mdi-maps-local-library prefix" style="font-size: 2em;"></i>
          </div>
          <div class="col m11">
            <label>Beasiswa</label>
            <select class="browser-default" id="beasiswa" name="beasiswa">
              <option value="" disabled selected>Pilih Beasiswa</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col s12">
            <div class="row">
              <div class="col m11">
                <label>Upload File</label>
              </div>
              <div class="file-field input-field col m11">
                <div class="col m6">
                 <div class="btn">
                  <i class="mdi-file-cloud-upload"></i>
                  <input type="file" name="file" id="file" title="Choose File" required="required"/>
                </div>
              </div>
              <div class="col m6">
               <input class="file-path validate" name="pathfile" type="text"/>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
      <div class="input-field col s12 right">
        <button class="btn-large waves-effect waves-light col s12 blue" type="submit" name="action1" value="Update">Simpan</button>
      </div>
    </div>
    <input type="hidden" name="nim" id="nim" value="<?php echo $this->session->userdata('username');?>">
    <input type="hidden" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d');?>">
  </div>
  <hr>
</form>
</div>
</div>

<!-- Modal Edit Bukti Transfer -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4 class="modal-title">Edit Bukti Transfer</h4>
    <hr><br>
    <form method="post" action="<?php echo base_url(); ?>mahasiswa/C_upload_transfer/update_data" id="formInput" enctype=multipart/form-data>
    <input type="hidden" name="idUpload" id="idUpload">
      <div class="row">
        <div class="row">
          <div class="col m1">
            <i class="mdi-av-timer prefix" style="font-size: 2em;"></i>
          </div>
          <div class="col m11">
            <label>Tahun</label>
            <select class="browser-default" name="tahunn" id="tahunn">
              <option value="" disabled selected>Pilih Tahun</option>
              <?php
              foreach ($tahun as $row) {
                ;?>
                <option value="<?php echo $row->tahun;?>"><?php echo $row->tahun;?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col m1">
              <i class="mdi-maps-local-library prefix" style="font-size: 2em;"></i>
            </div>
            <div class="col m11">
              <label>Beasiswa</label>
              <select class="browser-default" id="beasiswaa" name="beasiswaa">
                <option value="" disabled selected>Pilih Beasiswa</option>
              </select>
            </div>
          </div>


          <div class="form-group">
            <div class="col s12">
              <div class="row">
                <div class="col m11">
                  <label>Change Upload File</label>
                </div>
                <div class="file-field input-field col m11">
                  <div class="col m6">
                   <div class="btn">
                    <i class="mdi-file-cloud-upload"></i>
                    <input type="file" name="file" id="file2" title="Choose File"/>
                  </div>
                </div>
                <div class="col m6">
                 <input class="file-path validate" name="pathfile" id="pathfile2" type="text"/>
               </div>
             </div>
           </div>
         </div>
       </div>
       <div class="row">
        <div class="input-field col s12 right">
          <button class="btn-large waves-effect waves-light col s12 blue" type="submit" value="Update">Simpan</button>
        </div>
      </div>
      <input type="hidden" name="tanggal" id="tanggal2">
    </div>
    <hr>
  </form>
</div>
</div>
