<!-- Main START -->
<main>
  <style media="screen">
    .informasi{
      position: fixed;
      float: right;
    }

    .isi-info{
      display: block;
      padding: 5px;
      color: #ffffff;
    }

    .jumlah-diterima{
      text-align: center;
      font-size: 15pt;
    }
  </style>
  <div class="alert primary-color informasi">
    <a href="javascript:;" onclick="viewDiterima">
      <span class="isi-info">
        Diterima
        <div class="jumlah-diterima">
          -
        </div>
      </span>
    </a>
  </div>

  <div class="container">
    <div id="dashboard">
      <div class="section">
        <div id="responsive" class="section">
          <div class="row">
            <div class="col m6 s12">
              <h4>
                Seleksi Penerima Beasiswa
              </h4>
            </div>
            </div>
          <div class="row">
            <div class="col s3">
              <label>Beasiswa</label>
              <select class="browser-default" name="filterBea" id="filterBea" onChange="viewTabel()">
                <option value="" disabled selected>Pilih Beasiswa</option>
                <?php
                $arr=0;
                foreach ($comboBea as $cb) {
                  echo "<option value='".$arr."-".$cb->id."'>".$cb->namaBeasiswa."</option>";
                  $arr+=1;
                }
                ?>
              </select>
            </div>
            <div class="col s3">
              <label>Fakultas</label>
              <select class="browser-default" name="fakultas" id="fakultas" onChange="viewTabel()">
                <option value="" disabled selected>Pilih Fakultas</option>
                <?php
                foreach ($comboFakultas as $cf) {
                  echo "<option value='".$cf->id."'>".$cf->namaFk."</option>";
                }
                ?>
              </select>
            </div>
            <div class="col s3">
              <label>Jurusan</label>
              <select class="browser-default" name="jurusan" id="jurusan" onChange="viewTabel()">
                <option value="" disabled selected>Pilih Jurusan</option>
              </select>
            </div>
            <div class="col s3">
              <label>Angkatan</label>
              <select class="browser-default"  id="angkatan" name="angkatan" onChange="viewTabel()">
                <option value="" disabled selected>Pilih Angkatan</option>
                <?php
                foreach ($comboAngkatan as $ca) {
                  echo "<option value='".$ca->angkatan."'>".$ca->angkatan."</option>";
                }
                ?>
              </select>
            </div>
          </div>
          <div class="row">
            <div id="colorText" class="red-text">
              <span id="infoSelektor"></span>
            </div>
          </div>

          <div class="row">
            <div class="col s12">
              <table class="striped" id="tabel">
                <thead>
                  <tr>
                    <th data-field="id" style="width: 3%;">#</th>
                    <th data-field="nim">NIM</th>
                    <th data-field="nama">Nama</th>
                    <th data-field="ipk">IPK</th>
                    <th data-field="skor">Skor</th>
                    <th data-field="jumlah">Nilai</th>
                    <th data-field="berkas">Berkas Upload</th>
                    <th data-field="updated">Updated</th>
                    <th data-field="aksi">Print Data</th>
                    <th data-field="aksi">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- container END -->

</main>
<script type="text/javascript">

  var dataTable;
  var idBea;
  var idFak;
  var idJur;
  var idAngkatan;
  var myVar;

  document.addEventListener("DOMContentLoaded", function(event) {
  //already
});

  function viewTabel() {
    dataArr = $("#filterBea").val().split("-");
    idBea = $("#filterBea").val();
    idFak = $("#fakultas").val();
    idJur = $("#jurusan").val();
    idAngkatan = $("#angkatan").val();
    this.idBea = dataArr[1];
    viewDetailBea(dataArr[0]);
    datatable();

    reloadJs('materialize', 'min');
    reloadJs('initialize', 'nomin');
  }

  function myTimer() {
    getDiterima();
    reload_table();
  }

  function viewDetailBea(indexArr) {
    if(indexArr!="kosong"){
      infoBea = <?php echo json_encode($comboBea)?>;
      selektor = infoBea[indexArr]['selektor'];
      if (selektor == "3") {
        $("#infoSelektor").html("[ 2 Selektor ] <a href='#' onclick='myTimer()' class='blue-text'>[ Refresh ]</a> ");
        $("#colorText").attr("class","red-text");
        this.myVar = setInterval(myTimer ,15000);
      } else {
        $("#infoSelektor").html("[ 1 Selektor ]");
        $("#colorText").attr("class","success-text");
        window.clearInterval(this.myVar);
      }
    } else {
      $("#infoSelektor").html("");
      window.clearInterval(this.myVar);
    }
    getDiterima();
  }

  function seleksi(idPendaftar, status, nim)
  {
    var url = "<?php echo site_url('staf_kemahasiswaan/C_seleksi/seleksi')?>";
    $.ajax({
      url : url+"/"+idPendaftar+"/"+status+"/"+nim,
      type: "POST",
      data: $('#formInput').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if (data.status==false) {
          alert("NIM: "+data.nim+", masih diterima di beasiswa '"+data.bea+"' yang periode berakirnya adalah "+data.periode_berakhir);
        }else {
          getDiterima();
          reload_table();
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding/update data');
      }
    });
  }

  function reload_table(){
    dataTable.ajax.reload(null,false);
  }

  function getDiterima()
  {
    if($("#filterBea").val()!="kosong"){
      dataArr = $("#filterBea").val().split("-");
      id_bea = dataArr[1];
      var url = "<?php echo site_url('staf_kemahasiswaan/C_seleksi/getDiterima')?>";
      $.ajax({
        url : url+"/"+id_bea,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          $(".jumlah-diterima").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data!');
        }
      });
    }else{
      $(".jumlah-diterima").html("-");
    }
  }

  function datatable() {
    dataTable = $('#tabel').DataTable({
      "destroy":true,
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url('staf_kemahasiswaan/C_seleksi/datatable'); ?>",
        type: "POST",
        data:{'idBea':idBea,'idFak':idFak,'idJur':idJur,'idAngkatan':idAngkatan}
      },
      "columnDefs":[
      {
        "targets":[0,-1],
        "orderable":false,
      },
      ],
      "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
      language : {
        sLengthMenu: "Tampilkan _MENU_",
        sSearch: "Cari"
      }
    });
  }

  function view_detail_score(idPendaftar,idBea) {
    var url = "<?php echo site_url('staf_kemahasiswaan/C_seleksi/view_detail_score')?>";
    $.ajax({
      url : url+"/"+idPendaftar+"/"+idBea,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        detail = '';
        total_skor_mhs=0;
        for (var i = 0; i < data.length; i++) {
          detail += `
          <tr>
            <td>`+data[i].kategori+`</td>
            <td>`+data[i].pilihan+`</td>
            <td>`+data[i].skor+`</td>
          </tr>
          `;
          total_skor_mhs += parseInt(data[i].skor);
        }
        $('#nim_mhs').html(data[0].nim);
        $('#detail_skor_mhs').html(detail);
        $('#total_skor_mhs').html(total_skor_mhs);
        $('#modal1').openModal();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data!');
      }
    });
  }

  function view_detail_upload(idPendaftar,idBea) {
    var url = "<?php echo site_url('staf_kemahasiswaan/C_seleksi/view_detail_upload')?>";
    $.ajax({
      url : url+"/"+idPendaftar+"/"+idBea,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        detail = '';
        no=0;
        for (var i = 0; i < data.length; i++) {
          no+=1;
          detail += `
          <tr>
            <td>`+no+`</td>
            <td>`+data[i].namaBerkas+`</td>
            <td>`+data[i].title+`</td>
            <td><a href="<?php echo site_url('assets/img/upload_berkas/`+data[i].title+`')?>" target="_blank"><img src="<?php echo site_url('assets/img/upload_berkas/kaca.png')?>" width="50" height="50"/></a></td>
          </tr>
          `;
        }
        $('#nim').html(data[0].nim);
        $('#detail_upload').html(detail);
        $('#modal2').openModal();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data!');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
   $('#fakultas').change(function(){
    var fakultas =  $('#fakultas').val();
    $.ajax({
      url: '<?php echo base_url('staf_kemahasiswaan/C_seleksi/getJurusan'); ?>',
      type: 'GET',
      data: "fakultas="+fakultas,
      dataType: 'json',
      success: function(data){
       var fakultas=`<select id="jurusan" name="jurusan">
       <option value="null">Pilihlah Jurusan</option>`;
       for (var i = 0; i < data.length; i++) {
        fakultas+='<option value="'+data[i].id+'">'+data[i].namaJur+'</option>';
      }
      fakultas+=`</select>
      <label>Jurusan</label>`;
      $('#jurusan').html(fakultas);
      reloadJs('materialize','min');
      reloadJs('initialize','nomin');
    }
  });
  });
 });
</script>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
    <h4>Detail Skor NIM : <span id="nim_mhs"></span> </h4>
    <hr><br>
    <div class="row">
      <table>
        <style media="screen">
          .fon{
            font-weight: bold;
          }
        </style>
        <thead style="font-weight: bold;">
          <tr>
            <td>Kategori</td>
            <td>Pilihan</td>
            <td>Skor</td>
          </tr>
        </thead>
        <tbody id="detail_skor_mhs">

        </tbody>
        <tfoot style="font-weight: bold;">
          <tr>
            <td></td>
            <td>Total</td>
            <td id="total_skor_mhs"></td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

<div id="modal2" class="modal">
  <div class="modal-content">
    <h4>Berkas Upload NIM : <span id="nim"></span> </h4>
    <hr><br>
    <div class="row">
      <div id="gallery">
        <table>
          <style media="screen">
            .fon{
              font-weight: bold;
            }
          </style>
          <thead style="font-weight: bold;">
            <tr>
              <td>No</td>
              <td>Nama Berkas</td>
              <td>File Name</td>
              <td>Lihat Berkas</td>
            </tr>
          </thead>
          <tbody id="detail_upload">

          </tbody>
          <tfoot style="font-weight: bold;">
            <tr>
              <td>No</td>
              <td>Nama Berkas</td>
              <td>File Name</td>
              <td>Lihat Berkas</td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
