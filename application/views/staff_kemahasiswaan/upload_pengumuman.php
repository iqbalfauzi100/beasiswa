<!-- Main START -->
<main>
  <div class="container">
    <div id="dashboard">
      <div class="section">
        <div id="responsive" class="section">
          <div class="row">
            <div class="col s12 m6">
              <h4>
                Upload Pengumuman
                <button class="btn-floating waves-effect waves-light primary-color z-depth-0" onclick="add_data()" title="Tambah Data"><i class="mdi-content-add"></i></button>
              </h4>
            </div>
          </div>
          <div class="col s12 m4 l6">
            <?php echo $this->session->flashdata('pesan');?></div>
            <div class="row">
              <div class="col s12">
                <table class="striped" id="tabel">
                  <thead>
                    <tr>
                      <th data-field="id" style="width: 3%;">#</th>
                      <th data-field="jenis_scoring">Jenis</th>
                      <th data-field="jenis_scoring">Judul</th>
                      <th data-field="jenis_scoring">Tanggal</th>
                      <th data-field="jenis_scoring">File</th>
                      <th data-field="aksi">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Rekening Listrik</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Pekerjaan Ayah</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Pekerjaan Ibu</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- container END -->
  </main>

  <script type="text/javascript">

    var save_method;
    var arr = 0;
    var dataTable;

    document.addEventListener("DOMContentLoaded", function(event) {
      datatable();
    });

    function datatable() {
      dataTable = $('#tabel').DataTable({
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
          url:"<?php echo base_url('staf_kemahasiswaan/C_upload_pengumuman/datatable'); ?>",
          type:"POST"
        },
        "columnDefs":[
        {
          "targets":[2,-1],
          "orderable":false,
        },
        ],
        "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
        language : {
          sLengthMenu: "Tampilkan _MENU_",
          sSearch: "Cari:"
        }
      });
    }

    function reload_table(){
      dataTable.ajax.reload(null,false);
    }

    function add_data() {
      arr = 0;
      save_method = 'add';
      $('#formInput')[0].reset();
      $('.form-group').removeClass('has-error');
      $('.blue-text').empty(); 
      $('#modal1').openModal();
    }

    function edit(id) {
      arr = 0;
      save_method = 'update';
      $('#formInput')[0].reset();

      $.ajax({
        url : "<?php echo site_url('staf_kemahasiswaan/C_upload_pengumuman/edit_data/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          $('#judul2').val(data[0].judul);
          $('#jenis2').val(data[0].jenis);
          $('#idPengumuman2').val(data[0].id);
          $('#tanggal2').val(data[0].tanggal);
          $('#pathfile2').val(data[0].file);
          
          reloadJs('materialize', 'min');
          reloadJs('initialize', 'nomin');
          $('#modal2').openModal();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data');
        }
      });
    }

    function remove(id, nama) {
      swal({
        title: '"'+nama+'"',
        text: "Apakah anda yakin ingin menghapus Pengumuman ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#F44336',
        confirmButtonText: 'Hapus',
        cancelButtonText: "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm){
          $.ajax({
            url : "<?php echo site_url('staf_kemahasiswaan/C_upload_pengumuman/delete_data/')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
              reload_table();
              swal("Terhapus :(", "Pengumuman telah berhasil dihapus!", "success");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              swal("Erorr!", "Terjadi masalah saat penghapusan data!", "error");
            }
          });
        } else {
          swal("Dibatalkan :)", "Penghapusan Pengumuman dibatalkan!", "error");
        }
      });
    }
  </script>

  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4 class="modal-title">Tambahkan Pengumuman</h4>
      <hr><br>
      <form method="post" action="<?php echo base_url(); ?>staf_kemahasiswaan/C_upload_pengumuman/save_data" id="formInput" enctype=multipart/form-data>
        <input type="hidden" id="idPengumuman" name="idPengumuman">
        <div class="row">
          <div class="form-group">
            <div class="input-field">
              <input id="jenis" type="text" name="jenis" class="validate" required="required">
              <label for="jenis">Jenis Surat</label>
            </div>  
          </div> 
          <div class="form-group">
            <div class="input-field">
              <input id="judul" type="text" name="judul" class="validate" required="required">
              <label for="judul">Judul</label>
            </div>  
          </div> 
          <div class="form-group">
            <div class="col s12">
              <div class="row">
                <div class="col m11">
                  <label>Upload File</label>
                </div>
                <div class="file-field input-field col m11">
                  <div class="col m6">
                   <div class="btn">
                    <i class="mdi-file-cloud-upload"></i>
                    <input type="file" name="file" id="file" title="Choose File" required="required"/>
                  </div>
                </div>
                <div class="col m6">
                 <input class="file-path validate" name="pathfile" type="text"/>
               </div>
             </div>
           </div>
         </div>
       </div>
       <div class="row">
        <div class="input-field col s12 right">
          <button class="btn-large waves-effect waves-light col s12 blue" type="submit" name="action1" value="Update">Simpan</button>
        </div>
      </div>
      <input type="hidden" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d');?>">
    </div>
    <hr>
  </form>
</div>
<!-- <div class="modal-footer">
  <button class="modal-action modal-close waves-effect green btn" id="uploadFile" onclick="save()">Simpan</button>
</div> -->
</div>

<!-- Modal Structure -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4 class="modal-title">Edit Pengumuman</h4>
    <hr><br>
    <form method="post" action="<?php echo base_url(); ?>staf_kemahasiswaan/C_upload_pengumuman/update_data" id="formInput" enctype=multipart/form-data>
      <input type="hidden" id="idPengumuman2" name="idPengumuman">
      <div class="row">
        <div class="form-group">
          <div class="input-field">
          <input id="jenis2" type="text" name="jenis" class="validate" required="required">
            <label for="jenis">Jenis Surat</label>
          </div>  
        </div> 
        <div class="form-group">
          <div class="input-field">
            <input id="judul2" type="text" name="judul" class="validate" required="required">
            <label for="judul">Judul</label>
          </div>  
        </div>
        <div class="form-group">
          <div class="col s12">
            <div class="row">
              <div class="col m11">
                <label>Change Upload File</label>
              </div>
              <div class="file-field input-field col m11">
                <div class="col m6">
                 <div class="btn">
                  <i class="mdi-file-cloud-upload"></i>
                  <input type="file" name="file" id="file2" title="Choose File"/>
                </div>
              </div>
              <div class="col m6">
               <input class="file-path validate" name="pathfile" id="pathfile2" type="text"/>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
      <div class="input-field col s12 right">
        <button class="btn-large waves-effect waves-light col s12 blue" type="submit" value="Update">Simpan</button>
      </div>
    </div>
    <input type="hidden" name="tanggal" id="tanggal2">
  </div>
  <hr>
</form>
</div>
</div>
