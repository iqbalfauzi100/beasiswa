<!-- Main START -->
<main>
  <div class="container">
    <div id="dashboard">
      <div class="section">
        <div id="responsive" class="section">
          <div class="row">
            <div class="col s6">
              <h4>
                List Beasiswa
                <a href="<?php echo base_url('staf_kemahasiswaan/C_requested/pengaturan')?>" class="btn-floating waves-effect waves-light primary-color z-depth-0" title="Tambah Data"><i class="mdi-content-add"></i></a>
              </h4>
            </div>
            <div class="col s6">
              <p>
                <blockquote>
                  <font size="4pt">Keterangan: </font><br> <i class="mdi-toggle-radio-button-on"></i> = Kasubag. Kemahasiswaan. <br> <i class="mdi-toggle-radio-button-off"></i> = Kasubag. Kemahasiswaan Fakultas. <br> <i class="mdi-action-stars"></i> = Keduanya.
                </blockquote>
              </p>
            </div>
          </div>
          <div class="col s12 m4 l6">
            <?php echo $this->session->flashdata('pesan');?>
          </div>
          <div class="row">
            <div class="col s12">
              <table class="striped" id="tabel">
                <thead>
                  <tr>
                    <th data-field="id" style="width: 3%;">#</th>
                    <th data-field="bea">Beasiswa</th>
                    <th data-field="penyelenggara">Penyelenggara</th>
                    <th data-field="selektor">Selektor</th>
                    <th data-field="konfirmasi">Keterangan</th>
                    <th data-field="status">Progress</th>
                    <th data-field="status">Status</th>
                    <th data-field="status">AddData</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>BRI</td>
                    <td>Bank BRI</td>
                    <td><i class="material-icons" title="Kasubag. Kemahasiswaan">radio_button_checked</i></td>
                    <td class="success-text">Telah Dikonfirmasi</td>
                    <td>
                      <a class="btn-floating waves-effect waves-light primary-color z-depth-0" title="Confirmed"><i class="material-icons">done</i></a>
                      </td
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Prestasi</td>
                      <td>UIN Maliki</td>
                      <td><i class="material-icons" title="Kasubag. Kemahasiswaan Fakultas">radio_button_unchecked</i></td>
                      <td class="alert-text" style="max-width: 330px;">Scoring pekerjaan orang tua belum masuk</td>
                      <td>
                        <form action="<?php echo base_url('staf_kemahasiswaan/C_requested/pengaturan')?>" method="post">
                          <button class="btn-floating waves-effect waves-light red" title="Not Confirmed" type="submit" name="idPengaturan" value="bismillah"><i class="material-icons">settings</i></button>
                        </form>
                        </td
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>BNI</td>
                        <td>Bank BNI</td>
                        <td>
                          <i class="material-icons" title="Keduanya">star</i>
                        </td>
                        <td class="alert-text" style="max-width: 330px;"></td>
                        <td>
                          <form action="<?php echo base_url('staf_kemahasiswaan/C_requested/pengaturan')?>" method="post">
                            <button class="btn-floating waves-effect waves-light red" title="Not Confirmed" type="submit" name="idPengaturan" value="bisa"><i class="material-icons">settings</i></button>
                          </form>
                          </td
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <!-- container END -->
      </main>
      <script type="text/javascript">

        var dataTable;

        document.addEventListener("DOMContentLoaded", function(event) {
          datatable();
        });

        function datatable() {
          dataTable = $('#tabel').DataTable({
            "processing":true,
            "serverSide":true,
            "order":[],
            "ajax":{
              url:"<?php echo base_url('staf_kemahasiswaan/C_requested/datatable'); ?>",
              type:"POST"
            },
            "columnDefs":[
            {
              "targets":[2,-1],
              "orderable":false,
            },
            ],
            "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
            language : {
              sLengthMenu: "Tampilkan _MENU_",
              sSearch: "Cari:"
            }
          });
        }
        function reload_table(){
          dataTable.ajax.reload(null,false);
        }
        
        function edit(id)
        {
          $('#formInput')[0].reset();

          $.ajax({
            url : "<?php echo site_url('staf_kemahasiswaan/C_requested/ajax_edit/')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
              $('[name="idBea"]').val(data.id);
              $("#progress").val(data.progress);
              $("#colorProgress").val(data.colorProgress);
              reloadJs('materialize', 'min');
              reloadJs('initialize', 'nomin');
              $('#modal1').openModal();

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error get data from ajax');
            }
          });
        }
        function save()
        {
          var url;
          url = "<?php echo site_url('staf_kemahasiswaan/C_requested/update_progress')?>";
          $.ajax({
            url : url,
            type: "POST",
            data: $('#formInput').serialize(),
            dataType: "JSON",
            success: function(data)
            {
             $('#modal1').closeModal();
             reload_table();
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
            alert('Error adding/update data');
          }
        });
        }

        function status(){
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'Anda tidak bisa menambah data, Beasiswa ini belum di ACC Kabag !'
          })
        }
      </script>

      <!-- Modal Structure -->
      <div id="modal1" class="modal">
        <div class="modal-content">
          <h4>Edit Progress Beasiswa</h4>
          <hr><br>
          <form action="#" id="formInput">
            <div class="row">
              <div class="col m1">
                <i class="mdi-maps-local-library prefix" style="font-size: 2em;"></i>
              </div>
              <div class="col m11">
                <label>Progress Beasiswa</label>
                <select name="progress" id="progress" class="browser-default" required="required">
                  <option value="1">1. Pendaftaran</option>
                  <option value="2">2. Seleksi</option>
                  <option value="3">3. Kelulusan</option>
                  <option value="4">4. Pencairan</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col m1">
                <i class="mdi-action-perm-identity prefix" style="font-size: 2em;"></i>
              </div>
              <div class="col m11">
                <label>Color Progress</label>
                <select name="colorProgress" id="colorProgress" class="browser-default" required="required">
                  <option value="1">1. Hijau</option>
                  <option value="2">2. Kuning</option>
                  <option value="3">3. Merah</option>
                </select>
              </div>
              <input type="hidden" name="idBea">
            </div>
            <div class="row">                        
              <div class="col s12">
                <b><p>Keterangan Color Progress:</p>
                  <ol>
                    <li><font color="green">Hijau = Belum Dilaksanakan</font></li>
                    <li><font color="yellow">Kuning = Dalam Proses</font></li>
                    <li><font color="red">Merah = Sudah Selesai</font></li>
                  </ol></b>
                </div>
                <div class="col s6">

                </div>
              </div>
              <hr>
            </form>
          </div>
          <div class="modal-footer">
            <button class="btn green" onclick="save()">Simpan</button>
          </div>
        </div>
