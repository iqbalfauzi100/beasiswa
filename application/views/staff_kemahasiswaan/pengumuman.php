<!-- Main START -->
<main>
  <div class="container">
    <div id="dashboard">
      <div class="section">
        <div id="responsive" class="section">
          <div class="row">
            <div class="col s12 m6">
              <h4>
                Daftar Pengumuman
              </h4>
            </div>
          </div>
            <div class="row">
              <div class="col s12">
                <table class="striped" id="tabel">
                  <thead>
                    <tr>
                      <th data-field="id" style="width: 3%;">#</th>
                      <th data-field="jenis_scoring">Jenis</th>
                      <th data-field="jenis_scoring">Judul</th>
                      <th data-field="jenis_scoring">Tanggal</th>
                      <th data-field="jenis_scoring">File</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Rekening Listrik</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Pekerjaan Ayah</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Pekerjaan Ibu</td>
                      <td>
                        <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                        <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- container END -->
  </main>

  <script type="text/javascript">

    var save_method;
    var arr = 0;
    var dataTable;

    document.addEventListener("DOMContentLoaded", function(event) {
      datatable();
    });

    function datatable() {
      dataTable = $('#tabel').DataTable({
        "processing":true,
        "serverSide":true,
        "order":[],
        "ajax":{
          url:"<?php echo base_url('staf_kemahasiswaan/C_pengumuman/viewPengumuman'); ?>",
          type:"POST"
        },
        "columnDefs":[
        {
          "targets":[2,-1],
          "orderable":false,
        },
        ],
        "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
        language : {
          sLengthMenu: "Tampilkan _MENU_",
          sSearch: "Cari:"
        }
      });
    }

    function reload_table(){
      dataTable.ajax.reload(null,false);
    }

  </script>
