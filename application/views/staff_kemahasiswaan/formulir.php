<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="MateRio is a responsive Admin Template based on Material Design by Google.">
  <meta name="keywords" content="materialize, admin template, dashboard template, responsive admin template,">
  <link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets/img/icons/favicons/uin.ico" />
  <title>Formulir</title>

  <link href="<?php echo base_url('assets/css/materialize.css')?>" type="text/css" rel="stylesheet" media="screen">
  <link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">
  <!-- Additional plugins styles -->
  <link href="<?php echo base_url('assets/css/plugins/prism.css')?>" type="text/css" rel="stylesheet" media="screen">
  <link href="<?php echo base_url('assets/css/plugins/simplebar.css')?>" type="text/css" rel="stylesheet" media="screen">
  <link href="<?php echo base_url('assets/css/assistance.css')?>" type="text/css" rel="stylesheet" media="screen">
  <style type="text/css">
    html,
    body{
      height: 100%;
    }
    main {
      padding: 0 !important;
    }
    .form-header {
      padding: 12px 24px 12px 12px;
    }
    .form-header .col{
      height:64px;
    }
    .form-body {
      padding: 12px 24px;
    }
  </style>
</head>

<body>

  <!-- Main Start -->
  <main class="valign-wrapper">
    <div class="container valign">

      <!--  Tables Section-->
      <div id="login" class="row">
        <!-- <h1 class="thin">Login</h1> -->
        <div class="col s12 m12 l12 card-panel">
          <form class="login-form" method="post" onsubmit="return cekform();" action="<?php echo base_url(); ?>staf_kemahasiswaan/C_daftar/simpan" enctype="multipart/form-data">

            <!-- <form id="formInput"> -->
            <div class="row primary-color form-header">
              <div class="col s2">
                <img src="<?php echo base_url('assets/img/favicon 64 uin.png')?>" alt="logo" class="logo responsive-img-height">
              </div>
              <div class="col s10 right-align">
                <h5 class="light white-text">Formulir Pendaftaran Beasiswa <span class="blue-text"> <?php echo $namaBea->namaBeasiswa; ?></span></h5>
              </div>
            </div>
            <div class="card-panel secondary-color white-text"><strong>PERHATIAN :</strong><br/>
              1. Sebelum Staff kemahasiswaan mengiputkan data, dimohon untuk setiap mahasiswa melengkapi profile data diri yang ada di laman Mahasiswa
            </div>
            <div class="row">
              <div class="col m6 s12">
                <div class="form-body">
                  <div class="row">
                    <div class="input-field col s12">
                      <i class="mdi-action-lock-open prefix"></i>
                      <input name="nim" id="username" type="text" class="validate" required="required">
                      <label for="username" class="center-align">NIM</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <i class="mdi-action-perm-identity prefix"></i>
                      <input id="email" name="nama" type="text" class="validate" required="required">
                      <label for="email" class="center-align">Nama</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s2 m2 l1">
                      <i class="mdi-action-turned-in-not prefix"></i>
                    </div>
                    <div class="input-field col s10 m10 l11">
                      <select name="fakultas" id="fakultas" required class="browser-default">
                        <option value="" disabled selected>Fakultas</option>
                        <?php foreach ($fakultas as $row) { ?>
                        <option value="<?php echo $row->id;?>"><?php echo $row->namaFk;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s2 m2 l1">
                      <i class="mdi-action-turned-in-not prefix"></i>
                    </div>
                    <div class="input-field col s10 m10 l11">
                      <select name="jurusan" id="jurusan" required class="browser-default">
                        <option value="" disabled selected>Jurusan</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s2 m2 l1">
                      <i class="mdi-action-turned-in-not prefix"></i>
                    </div>
                    <div class="input-field col s10 m10 l11">
                      <select name="semester" id="semester" required class="browser-default">
                        <option value="" disabled selected>-Pilihan Semester</option>
                        <?php
                        for ($i=1; $i < 15; $i++) {
                          echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                        ?>
                      </select>
                      <!-- <label>Semester</label> -->
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <i class="mdi-action-assignment prefix"></i>
                      <input name="sks" id="sks" type="tel" class="validate" onkeyup="validAngka(this)" maxlength="4" required="required">
                      <label for="username" class="center-align">SKS</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <i class="mdi-action-book prefix"></i>
                      <input name="ipk" id="username" type="tel" class="validate" onkeyup="validAngka(this)" maxlength="4" required="required">
                      <label for="username" class="center-align">IPK</label>
                      <small class="blue-text"><p>** Diharapkan menggunakan tanda . (Titik) bukan koma (,)</p>
                        <p>* Contoh IPK : 3.75</p>
                      </small>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <i class="mdi-action-markunread-mailbox prefix"></i>
                      <input name="alamatMalang" id="username" type="text" class="validate" required="required">
                      <label for="username" class="center-align">Alamat di Malang</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <i class="mdi-action-settings-cell prefix"></i>
                      <input name="noTelp" id="username" type="tel" class="validate" onkeyup="validAngka(this)" maxlength="12" required="required">
                      <label for="username" class="center-align">Nomor Hp/Telp</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col m6 s12">
                <div class="form-body">

                 <?php
                 echo $combo;
                 ?>
                 <br>
                 <small class="red-text">
                  .: Ketentuan Upload Berkas :.<br>
                </small>
                <?php
                echo $berkas;
                ?>

                <div class="row">
                  <small class="blue-text">
                    **Pastikan semua data yang anda masukkan adalah benar. <br>
                    **Dengan meng-klik "Daftar Sekarang", berarti anda telah menyetuji prasyarat dan ketentuan yang telah ditetapkan oleh Kemahasiswaan.<br>**Seluruh isian form wajib diisi.
                  </small>
                  <div class="input-field col s12 right">
                    <button class="btn-large waves-effect waves-light col s12 blue" type="submit" name="action1" value="Update">Daftar Sekarang</button>
                  </div>
                  <input name="idBea" id="idBea" type="hidden" class="validate" value="<?php echo $idBea?>">
                  <input name="tanggal" id="tanggal" type="hidden" value="<?php echo date("Y-m-d");?>">
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div><!-- container end -->

</main>
<!-- Main End -->

  <!-- -------------------------------------------------+
  Template Scripts
  +-------------------------------------------------- -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/bin/jquery-2.1.4.min.js')?>"></script>
  <script>if (!window.jQuery) { document.write('<script src="<?php echo base_url('assets/js/bin/jquery-2.1.4.min.js')?>"><\/script>'); }</script>
  <!-- jQuery Plugins -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/bin/plugins/prism.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bin/plugins/simplebar.min.js')?>"></script>
  <!--materialize js-->
  <script type="text/javascript" src="<?php echo base_url('assets/js/bin/materialize.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bin/initialize.js')?>"></script>
  <!-- ScrollFire initialize -->

</body>
<script type="text/javascript">

  function save(){
    $('#btnSave').text('saving...');
    $('#btnSave').attr('disabled',true);

    var url;
    url = "<?php echo site_url('staf_kemahasiswaan/C_daftar/simpan')?>";

    $.ajax({
      url : url,
      type: "POST",
      data: $('#formInput').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        window.location.href="<?php echo base_url('staf_kemahasiswaan/C_requested')?>";
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled',false);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding/update data');
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled',false);
      }
    });
  }

  function cekform() {
   if(!$("#selecton").val())
   {
    swal ( "Pilih salah satu!!!" ,  "Harus dipilih!" ,  "error" );
    $("#selecton").focus()
    return false;
  }

}
function validAngka(a)
{
  if(!/^[0-9.]+$/.test(a.value))
  {
   a.value = a.value.substring(0,a.value.length-1000);
 }
}

$(document).ready(function(){
 $('#fakultas').change(function(){
  var fakultas =  $('#fakultas').val();
  $.ajax({
    url: '<?php echo base_url('staf_kemahasiswaan/C_staff/getJurusan'); ?>',
    type: 'GET',
    data: "fakultas="+fakultas,
    dataType: 'json',
    success: function(data){
     var fakultas=`<select id="jurusan" name="jurusan">
     <option value="null">Pilihlah Jurusan</option>`;
     for (var i = 0; i < data.length; i++) {
      fakultas+='<option value="'+data[i].id+'">'+data[i].namaJur+'</option>';
    }
    fakultas+=`</select>
    <label>Jurusan</label>`;
    $('#jurusan').html(fakultas);
    reloadJs('materialize','min');
    reloadJs('initialize','nomin');
  }
});
});
});
</script>
</html>
