<!-- Main START -->
<main>
  <div class="container">
    <div id="dashboard">
      <div class="section">
        <div id="responsive" class="section">
          <div class="row">
            <div class="col s12 m6">
              <h4>
                Master Upload Berkas
                <button class="btn-floating waves-effect waves-light primary-color z-depth-0" onclick="add_data()" title="Tambah Data"><i class="mdi-content-add"></i></button>
              </h4>
            </div>
          </div>
          <div class="row">
            <div class="col s12">
              <table class="striped" id="tabel">
                <thead>
                  <tr>
                    <th data-field="id" style="width: 3%;">#</th>
                    <th data-field="jenis_scoring">Nama Berkas</th>
                    <th data-field="aksi">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Rekening Listrik</td>
                    <td>
                      <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                      <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Pekerjaan Ayah</td>
                    <td>
                      <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                      <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                    </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Pekerjaan Ibu</td>
                    <td>
                      <a class="btn-floating waves-effect waves-light yellow accent-4" title="Edit"><i class="material-icons">mode_edit</i></a>
                      <a class="btn-floating waves-effect waves-light red" title="Hapus"><i class="material-icons">delete</i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- container END -->
</main>

<script type="text/javascript">

  var save_method;
  var arr = 0;
  var dataTable;

  document.addEventListener("DOMContentLoaded", function(event) {
    datatable();
  });

  function datatable() {
    dataTable = $('#tabel').DataTable({
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url('staf_kemahasiswaan/C_master_upload/datatable'); ?>",
        type:"POST"
      },
      "columnDefs":[
      {
        "targets":[2,-1],
        "orderable":false,
      },
      ],
      "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
      language : {
        sLengthMenu: "Tampilkan _MENU_",
        sSearch: "Cari:"
      }
    });
  }

  function reload_table(){
    dataTable.ajax.reload(null,false);
  }

  function add_data() {
    arr = 0;
    save_method = 'add';
    $('#formInput')[0].reset();
    $('#modal1').openModal();
  }

  function save()
  {
    var url;
    if(save_method == 'add')
    {
      url = "<?php echo site_url('staf_kemahasiswaan/C_master_upload/add_data')?>";
    }
    else
    {
      url = "<?php echo site_url('staf_kemahasiswaan/C_master_upload/update_data')?>";
    }
    $.ajax({
      url : url,
      type: "POST",
      data: $('#formInput').serialize(),
      dataType: "JSON",
      success: function(data)
      {
       $('#modal1').closeModal();
       reload_table();
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
      alert('Error adding/update data');
    }
  });
  }

  function edit(id) {
    arr = 0;
    save_method = 'update';
    $('#formInput')[0].reset();

    $.ajax({
      url : "<?php echo site_url('staf_kemahasiswaan/C_master_upload/edit_data/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#namaBerkas').val(data[0].namaBerkas);
        $('#idNamaBerkas').val(data[0].id);

          reloadJs('materialize', 'min');
          reloadJs('initialize', 'nomin');
          $('#modal1').openModal();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data');
        }
      });
  }

  function remove(id, nama) {
    swal({
      title: '"'+nama+'"',
      text: "Apakah anda yakin ingin menghapus nama berkas ini?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#F44336',
      confirmButtonText: 'Hapus',
      cancelButtonText: "Batal",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm){
        $.ajax({
          url : "<?php echo site_url('staf_kemahasiswaan/C_master_upload/delete_data')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
            reload_table();
            swal("Terhapus :(", "Jenis scoring pilihan telah berhasil dihapus!", "success");
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            swal("Erorr!", "Terjadi masalah saat penghapusan data!", "error");
          }
        });
      } else {
        swal("Dibatalkan :)", "Penghapusan jenis scoring dibatalkan!", "error");
      }
    });
  }
</script>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
    <h4>Tambah Master Upload Berkas</h4>
    <hr><br>
    <form action="#" id="formInput">
      <div class="">
        <div class="row">
          <div class="input-field">
            <input type="hidden" id="idNamaBerkas" name="idNamaBerkas">
            <input id="namaBerkas" type="text" name="namaBerkas" class="validate">
            <label for="namaBerkas">Nama Berkas</label>
            </div>
        </div>
        <hr>
      </form>
    </div>
    <div class="modal-footer">
      <button class=" modal-action modal-close waves-effect green btn" onclick="save()">Simpan</button>
    </div>
  </div>
